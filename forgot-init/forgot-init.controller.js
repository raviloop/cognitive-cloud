(function () {
    'use strict';

    angular.module('app')
        .controller('ForgotInitController', ForgotInitController);

    ForgotInitController.$inject = ['$location','$routeParams', 'PasswordService', 'FlashService'];
    function ForgotInitController($location,$routeParams, PasswordService, FlashService) {
        var vm = this;

        var username="";
        vm.username=username;
        var param="";
        vm.param=$routeParams.key;

        var initForgotPassword = function(){
          var isValidForm = $('#forgot-init-form').data('formValidation').isValid();
          if (isValidForm) {
            console.log(this.username);
            PasswordService.initForgotPassword(this.username).then(
              function(response){
                $(".alert-danger").fadeOut()
                $(".alert-success").fadeIn();

                window.setTimeout(function () {
                  location.href = "/#!login";
                }, 3000);
              },
              function(response){
                $(".alert-success").fadeOut();
                $(".alert-danger").fadeIn()
              }
            )

          }
        }

        vm.initForgotPassword=initForgotPassword

    }

})();
