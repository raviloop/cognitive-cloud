(function () {
    'use strict';

    angular
        .module('app')
        .factory('UserService', UserService);

    UserService.$inject = ['$timeout', '$filter', '$q', '$http','$rootScope'];
    function UserService($timeout, $filter, $q, $http, $rootScope) {
        var service = {};
        service.GetAll = GetAll;
        service.GetById = GetById;
        service.GetByUsername = GetByUsername;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;

        return service;

        function GetAll() {
            return $http.get(UAA_MICROSERVICE_ENDPOINT+'/api/users?access_token='+$rootScope.globals.currentUser.accessToken).then(handleSuccess, handleError('Error getting all users'));
        }

        function GetById(id) {
            return $http.get(UAA_MICROSERVICE_ENDPOINT+'/api/users/' + id).then(handleSuccess, handleError('Error getting user by id'));
        }

        function GetByUsername(username) {
            /*return $http.post(UAA_MICROSERVICE_ENDPOINT+'/oauth/token'+'?username='+username+'&grant_type=password',
             JSON.stringify({ username: username, password: password })).then(handleSuccess, handleError('Error getting user by username'));*/
             return $http({
                 method: 'GET',
                 url: UAA_MICROSERVICE_ENDPOINT + '/api/account',
                 headers: {
                     'Content-Type': 'application/json',
                     'Authorization': 'Bearer '+$rootScope.globals.currentUser.accessToken
                 }
             });
            // return $http.get(UAA_MICROSERVICE_ENDPOINT+'/api/account?Bearer '
        }

        function Create(user) {
			user.login = user.email;
			user.planId = 2;
           //return $http.post(UAA_MICROSERVICE_ENDPOINT+'/api/register?access_token='+$rootScope.globals.currentUser.accessToken, user).then(handleSuccess, handleError('Error creating user'));
           return $http.post(UAA_MICROSERVICE_ENDPOINT+'/api/register', user);
        }

        function Update(user) {
            return $http.put('/api/users/' + user.id, user).then(handleSuccess, handleError('Error updating user'));
        }

        function Delete(id) {
            return $http.delete('/api/users/' + id).then(handleSuccess, handleError('Error deleting user'));
        }

        // private functions

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }

})();
