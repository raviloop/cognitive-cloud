(function () {
    'use strict';

    angular
        .module('app')
        .factory('TeamService', TeamService);

	angular.module('app').config(['$qProvider', function ($qProvider) {
		$qProvider.errorOnUnhandledRejections(false);
	}]);

    TeamService.$inject = ['$http', '$rootScope'];
    function TeamService($http, $rootScope) {
        //console.log(JSON.stringify($rootScope.globals.currentUser));


        var service = {};
         service.GetAll = GetAll;
         service.GetAllWithRole = GetAllWithRole;
         service.GetById = GetById;
         //service.GetByUsername = GetByUsername;
         service.Create = Create;
         service.Update = Update;
         service.Delete = Delete;
        return service;

         function GetAll() {
             //console.log($rootScope);
             return $http({
                 method: 'GET',
                 url: USER_MGMT_MICROSERICE_ENDPOINT + '/api/teamsByUser?access_token='+$rootScope.globals.currentUser.accessToken,
                 headers: {
                     'Content-Type': 'application/json',
                     'Authorization': 'Bearer '+$rootScope.globals.currentUser.accessToken
                 },
                 withCredentials:true
             });

         }

         function GetAllWithRole(role) {
             //console.log($rootScope);
             return $http({
                 method: 'GET',
                 url: USER_MGMT_MICROSERICE_ENDPOINT + '/api/teamsByUserAndRole?access_token='+$rootScope.globals.currentUser.accessToken,
                 headers: {
                     'Content-Type': 'application/json',
                     'Authorization': 'Bearer '+$rootScope.globals.currentUser.accessToken
                 },
                 withCredentials:true
             });

         }

         function GetById(id) {
             return $http({
                 method: 'GET',
                 url: USER_MGMT_MICROSERICE_ENDPOINT + '/api/teams'+id+'?access_token='+$rootScope.globals.currentUser.accessToken,
                 headers: {
                     'Content-Type': 'application/json',
                     'Authorization': 'Bearer '+$rootScope.globals.currentUser.accessToken
                 },
                 withCredentials:true
             });
         }

         function Create(teams,successCallback,errorCallback) {


            $http({
                 method: 'POST',
                 url: USER_MGMT_MICROSERICE_ENDPOINT + '/api/teams?access_token=' + $rootScope.globals.currentUser.accessToken,
                 headers: {
                     'Content-Type': 'application/json',
                     'Authorization': 'Bearer '+$rootScope.globals.currentUser.accessToken,
                     'Accept' : 'application/json'
                 },
                 withCredentials:true,
                data: teams,
             }).then(function (response) {
                 successCallback(response);
             }, function (response) {
                errorCallback(response);
             });


        }

         function Update(team,successCallback,errorCallback) {

			 $http({
                 method: 'PUT',
                 url: USER_MGMT_MICROSERICE_ENDPOINT + '/api/teams?access_token=' + $rootScope.globals.currentUser.accessToken,
                 headers: {
                     'Content-Type': 'application/json',
                     'Authorization': 'Bearer '+$rootScope.globals.currentUser.accessToken,
                     'Accept' : 'application/json'
                 },
                data: team,
             }).then(function (response) {
                successCallback(response);
             }, function (response) {
                errorCallback(response)
             });
         }

         function Delete(id) {
             return $http({
                 method: 'DELETE',
                 url: USER_MGMT_MICROSERICE_ENDPOINT + '/api/teams/'+id+'?access_token=' + $rootScope.globals.currentUser.accessToken,
                 headers: {
                   'Content-Type': 'application/json',
                   'Authorization': 'Bearer '+$rootScope.globals.currentUser.accessToken,
                   'Accept' : 'application/json'
                 },
                data: id
             });
         }

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }
})();
