(function () {
    'use strict';

    angular
        .module('app')
        .factory('PasswordService', PasswordService);

    PasswordService.$inject = ['$http', '$cookies', '$rootScope', '$timeout', 'UserService'];
    function PasswordService($http, $cookies, $rootScope, $timeout, UserService) {
        var service = {};

        service.initForgotPassword = initForgotPassword;
        service.completeForgotPassword=completeForgotPassword;

        return service;
        function completeForgotPassword(key,password){
          return $http.post(UAA_MICROSERVICE_ENDPOINT+'/api/account/reset_password/finish',JSON.stringify({key:key,newPassword:password}));
        }

        function initForgotPassword(username) {


			return $http.post(UAA_MICROSERVICE_ENDPOINT+'/api/account/reset_password/init',username);

        }



    }



})();
