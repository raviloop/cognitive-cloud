(function () {
    'use strict';

    angular
        .module('app')
        .factory('ServiceStoresService', ServiceStoresService);

	angular.module('app').config(['$qProvider', function ($qProvider) {
		$qProvider.errorOnUnhandledRejections(false);
	}]);

    ServiceStoresService.$inject = ['$http', '$rootScope'];
    function ServiceStoresService($http, $rootScope) {


        var service = {};
         service.GetAll = GetAll;

         service.Create = Create;

        return service;

         function GetAll(successCallback,errorCallback) {
             //console.log($rootScope);
              $http({
                 method: 'GET',
                 url: USER_MGMT_MICROSERICE_ENDPOINT + '/api/service-stores?access_token='+$rootScope.globals.currentUser.accessToken,
                 headers: {
                     'Content-Type': 'application/json',
                     'Authorization': 'Bearer '+$rootScope.globals.currentUser.accessToken
                 },
                 withCredentials:true
             }).then(function (response) {
                 successCallback(response);
             }, function (response) {
                errorCallback(response);
             });;

         }



         function Create(teamId, storeId,successCallback,errorCallback) {


            $http({
                 method: 'POST',
                 url: USER_MGMT_MICROSERICE_ENDPOINT + '/api/addServiceStoreToTeam?access_token=' + $rootScope.globals.currentUser.accessToken,
                 headers: {
                     'Content-Type': 'application/json',
                     'Authorization': 'Bearer '+$rootScope.globals.currentUser.accessToken,
                     'Accept' : 'application/json'
                 },
                 withCredentials:true,
                data: {teamId:teamId,storeId:storeId},
             }).then(function (response) {
                 successCallback(response);
             }, function (response) {
                errorCallback(response);
             });


        }



        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }
})();
