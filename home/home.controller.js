(function () {
    'use strict';

    angular.module('app')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['TeamService','ServiceStoresService',  'UserService', '$rootScope', '$scope','$window','$location'];


    function HomeController( TeamService, ServiceStoresService,  UserService, $rootScope, $scope,$window,$location) {
        var vm = this;
        vm.user = null;
        vm.allUsers = [];
        vm.insertingTeam = {teamUsers:[{}]}

        initController();

        function initController() {
            loadCurrentUser();
            console.log("------------------------------")
            console.log(vm.insertingTeam);
        }

        // Form validation pattern
        $scope.wordPattern = /^[a-z]+$/i;


        //Create Team, Edit Team, Delete Team

        $scope.team = [];
        $scope.myNumber = 5;

        $scope.getNumber = function(num) {
        //console.log("getNumber Was Called "+num);
            return new Array(num);
        }

        $scope.getAllTeam = function() {

            console.log("get team data was called");
            $scope.allTeamData = [];

            TeamService.GetAll().then(function successCallback(response) {
                console.log("Success");
                console.log(response);
                $scope.allTeamData = response.data;
                $scope.allTeamDataBkp = angular.copy($scope.allTeamData);
                //console.log(angular.toJson($scope.allTeamData));
                //console.log(angular.toJson($scope.allTeamData));
            }, function errorCallback(response) {
                console.log("Error will retriving data");
            });
        }

        $scope.addTeamUserToInserting = function(){
          vm.insertingTeam.teamUsers.push({});
          //$('#createNewTeamForm').formValidation();

        }
        $scope.removeTeamUserToInserting = function(index){
            vm.insertingTeam.teamUsers.splice(index,1);
        }

        $scope.createTeam = function() {
            console.log(vm.insertingTeam);
            
            //validateCreateTeamForm();
           
            //var isValidForm = $('#createNewTeamForm').data('formValidation').isValid();
            //alert (isValidForm);
            var isValidForm = $scope.createNewTeamForm.$valid;
            if (isValidForm) {
              TeamService.Create(vm.insertingTeam, function(response){
                            $(".alert").fadeIn();
                            $location.path("/control-center");

                });
          }

        }

        $scope.resetTeamsEdit = function(){
          $scope.allTeamData= angular.copy($scope.allTeamDataBkp);
        }
        $scope.makeTeamEditable = function(data, index) {
          $("#accordion-control-"+index).collapse();
          data.isTeamEnabled = !data.isTeamEnabled;
        }

        $scope.editTeam = function(index, teamId) {
            console.log($scope.allTeamData);

            var team = $scope.allTeamData[index];
            TeamService.Update(team, function(response){
              $window.location.reload();
            });
        }

        $scope.addTeamUser = function (index){
                    $scope.allTeamData[index].teamUsers.push({});
        }

        $scope.removeTeamUser = function (index, teamuser){
                    $scope.allTeamData[index].teamUsers.splice($scope.allTeamData[index].teamUsers.indexOf(teamuser),1);
        }

        $scope.deleteTeam = function(index) {
            event.preventDefault();
            TeamService.Delete(index).then(function successCallback(response) {
                console.log("Success");
                console.log(response);
                $window.location.reload();
            }, function errorCallback(response) {
                console.log("Error will retriving data");
            });
        }

        function loadCurrentUser() {
            UserService.GetByUsername($rootScope.globals.currentUser.username)
                .then(function (user) {
                    vm.user = user.data;
                    if(vm.user.authorities[0]==="ROLE_ADMIN"){
                      vm.isAdmin = true;
                      vm.isUser=false;
                    }
                    else{
                      {
                        vm.isAdmin = false;
                        vm.isUser=true;
                      }
                    }
                    $scope.getAllServices();
                });

        };

        $scope.signout = function() {
            $window.localStorage.clear();
            $rootScope.globals = {};
        };
    }

})();
