

$(window).on("load",function(){
  $("#header").load("includes/header.html");
  //$("#left-bar").load("includes/left-bar.tpl");
  $("#footer").load("includes/footer.tpl");
});

/***show Top section in user_control_cnter */
function showServiceTopScreen(sectionName) {
  $(".section").fadeOut("fast",function(){
    $("#"+sectionName).fadeIn();
  })
}

$(document).ready(function() {
  setFeaturedSlideHeight();

  var windowHeight = $(window).height();
	var panelHeadingHeight = $(".panelStoreSticky .panel-heading").height();
	var panelbtntoggle = $(".panelStoreSticky .panel-heading #btn-toggle-Store").outerHeight();
	var panelStoreStickyTop = panelHeadingHeight+panelbtntoggle;
	$(".panelStoreSticky").height(panelHeadingHeight);

	var headerHeight = 66;//page header height is fixed
	var heightToExtend = (windowHeight -headerHeight) - panelbtntoggle;

	/**setting ss section height */
	var ssPanelSectionHeight = heightToExtend - panelHeadingHeight;
	$(".panelStoreSticky .section").height(ssPanelSectionHeight);

	var panelStoreStickyFlag = 0;
	$(".panelStoreSticky .panel-heading #btn-toggle-Store").click(function() {
		if(panelStoreStickyFlag === 0) {
			showServiceStore();
		}
		else
		{
			$(".panelStoreSticky").height(panelHeadingHeight);
			panelStoreStickyFlag = 0;
			$(this).find("i").removeClass("ion-chevron-down");
			$(this).find("i").addClass("ion-chevron-up");
			$("html,body").css("overflow","auto");
		}
		
	});

	$(".panelStoreSticky .panel-heading .form-control").keyup(function() {
		var searchLength = $(this).val().length;
		alert("service store");
		if(searchLength != 0) {
			showServiceStore();
		}
		
		
	});

	$(".userSerivce .blankCard").click(function() {
		showServiceStore();
	})
function showServiceStore() {
	$(".panelStoreSticky").height(heightToExtend);
	$(".panelStoreSticky #btn-toggle-Store").find("i").removeClass("ssArrowAnim");
	$(".panelStoreSticky #btn-toggle-Store").find("i").removeClass("ion-chevron-up");
	$(".panelStoreSticky #btn-toggle-Store").find("i").addClass("ion-chevron-down");

	$("html,body").css("overflow","hidden");
	panelStoreStickyFlag = 1;
}	

});





function setFeaturedSlideHeight() {
  var fNavHeight =$(".tf_section").height();
  $(".tf_section .tab-content").height(fNavHeight);
}

function changeAsideBar() {
    $(".panelStoreSticky").toggleClass("navunfolded");
}

$(".btnEdit").click(function() {
	$(this).closest(".panelStoreSticky .panel-heading").find(".panel-title").click();
});


/***add new members */
var counter = 0;
$("#btnAddNewMembers").click(function(){
	if(counter < 4) {
		var tablename = $("#createteamTable");
		var cloneElement = tablename.find("tbody tr:first-child").clone();
		cloneElement.find("td").css("border-top",0);
		cloneElement.find("td .form-control").val("");
		cloneElement.find("td .checkbox").attr("checked",false);
		cloneElement.find("td.tstoreAccess input").attr("id","checkbox"+counter);
		cloneElement.find("td.tstoreAccess label").attr("for","checkbox"+counter);

		var whereToAppend = tablename.find("tbody");
		whereToAppend.append(cloneElement);

		counter++;
	}
	else
	{
		$(this).hide();
	}

});


/****test automation */

