
////////////////////////// Services //////////////////////////
var jqServices = (function () {
    'use strict';
    var access_token = localStorage.accessToken; /////////
    var UAA_MICROSERVICE_ENDPOINT = "http://52.40.247.241:30313";
    var USER_MGMT_MICROSERICE_ENDPOINT = "http://52.40.247.241:30314";
    var TEST_AUTOMATION_ENDPOINT = "http://52.40.247.241:30411";

    //
    var getAllUsers = function () {
        var jqXHR = $.ajax({
            method: "GET",
            url: UAA_MICROSERVICE_ENDPOINT + '/api/users',
            data: '',
            //dataType: "json",
            cache: false,
            crossDomain: false, // default: false
            headers: {
                Authorization: 'Bearer ' + access_token
            },
            beforeSend: function (jqxhr, settings) {

            },
            success: function (data, status, jqxhr) {
                // create users list and prepend
                var subNavBtn = $('.addMemberGrp .nav-sub .btn');
                var str = '<ul class="nav text-sm">';

                $.grep(data, function (user) {
                    //console.log(user);
                    var userid = user.id;
                    var fullName = user.firstName + ' ' + user.lastName;
                    var userEmail = user.email;

                    str += '<li class="nav-item">';
                    str += '<a class="nav-link" href="javascript:void(0)">';
                    str += '<label class="md-check"><input type="checkbox" value="' + fullName + '" data-fullName="' + fullName + '" data-userid="' + userid + '"class="has-value"><i class="blue"></i>';
                    str += '<span class="capitalize">' + fullName + '</span>';
                    str += '</label></a></li>';
                });
                str += '</ul>';

                $(str).insertBefore(subNavBtn);
                console.log('success');
            },
            complete: function (jqxhr, status) {
                //console.log('status : ' + status);
            },
            error: function (jqxhr, status, errorThrown) {
                console.log('Error while getting all users : ' + status + " : " + errorThrown);
            }
        });
    };

    //createProject
    var createProject = function (objProject) {
        var returnObj = undefined;
        var jqXHR = $.ajax({
            method: "POST",
            url: TEST_AUTOMATION_ENDPOINT + '/api/projects?access_token=' + access_token,     ////////
            data: JSON.stringify(objProject), /////
            dataType: "json", //// 
            cache: false,
            async: false, ///// default : true
            crossDomain: false, // default: false
            headers: {
                Authorization: 'Bearer ' + access_token,
                'Content-Type' : "application/json"  ///////////
            },
            success: function (data, status, jqxhr) {
                // save in localStorage 
                //console.log('Success - Create Project: ' + JSON.stringify(data));
                //localStorage.setItem('Project', data); 
                returnObj = data;                
            },
            error: function (jqxhr, status, errorThrown) {
                console.log('Error - Create Project : ' + status + " : " + errorThrown);
            }
        });

        //jqXHR.success(function (data) {
        //    console.log('success 2 ==== ' + data);
        //    returnObj = data;
        //});

        return returnObj;
        
    };

    // updateProject
    var updateProject = function (objProject) {
        var returnObj = undefined;
        var jqXHR = $.ajax({
            method: "PUT",
            url: TEST_AUTOMATION_ENDPOINT + '/api/projects?access_token=' + access_token,     ////////
            data: JSON.stringify(objProject), /////
            dataType: "json", //// 
            cache: false,
            async: false, ///// default : true
            crossDomain: false, // default: false
            headers: {
                Authorization: 'Bearer ' + access_token,
                'Content-Type': "application/json"  ///////////
            },
            success: function (data, status, jqxhr) {               
                returnObj = data;
            },
            error: function (jqxhr, status, errorThrown) {
                console.log('Error - createTestSuite : ' + status + " : " + errorThrown);
            }
        });
         
        return returnObj;

    };

    return { getAllUsers: getAllUsers, createProject: createProject, updateProject: updateProject }
}());

/////////////////////////// Test Automation //////////////////

var testAutomation = (function (jqServices) {
    'use strict';
    //console.log('TestAutomation');

    // TestAutomation Page 1
    var createProject = function () {
        console.log('createProject');

        // Ajax call to get list of users 
        jqServices.getAllUsers();

        var frmCreateProject = $('#frmCreateProject');
        var btnCreateProject = $('#btnCreateProject');

        //
        btnCreateProject.on('click', function (e) {
            //e.stopPropagation();
            e.preventDefault();
            console.log(e);
            console.log('btnCreateProject');

            /////                         
            var tagLabels = $('.tagLabel');
            var rolesLink = $('#projectRoles a.nav-link');

            /////
            var tagsArray = [];
            var rolesArray = [];


            // Validation
            if ($('#projectName').val().length) {
                console.log('frmCreateProject valid ');

                // get all Tags
                tagLabels.each(function () {
                    var tag = $(this).data('tag');
                    tagsArray.push(tag);
                    //console.log(tagsArray);
                });

                //get all Roles
                //console.log(rolesLink);
                rolesLink.each(function () {
                    var cur = $(this);
                    var roleName = cur.data('role-name');
                    var tabpane = $(cur.attr('href'));
                    var users = [];

                    //console.log(tabpane);

                    $('.memberAddedList .card-title', tabpane).each(function () {
                        console.log($(this).data('userid'));
                        //users.push($(this).data('userid'));
                        var user = { userid: $(this).data('userid') };
                        users.push(user);
                    });


                    //TestCase
                    //var viewTestCase = $('[data-permission="viewTestCase"]', tabpane);
                    //var createTestCase = $('[data-permission="createTestCase"]', tabpane);
                    //var editTestCase = $('[data-permission="editTestCase"]', tabpane);
                    //var deleteTestCase = $('[data-permission="deleteTestCase"]', tabpane);

                    // TestScript
                    var viewTestScript = $('[data-permission="viewTestScript"]', tabpane);
                    var createTestScript = $('[data-permission="createTestScript"]', tabpane);
                    var editTestScript = $('[data-permission="editTestScript"]', tabpane);
                    var deleteTestScript = $('[data-permission="deleteTestScript"]', tabpane);

                    // TestSuite
                    var viewTestSuite = $('[data-permission="viewTestSuite"]', tabpane);
                    var createTestSuite = $('[data-permission="createTestSuite"]', tabpane);
                    var editTestSuite = $('[data-permission="editTestSuite"]', tabpane);
                    var deleteTestSuite = $('[data-permission="deleteTestSuite"]', tabpane);

                    // Users

                    //console.log(roleName);

                    // get all permissions of the current role
                    rolesArray.push({
                        'roleName': roleName,
                        // TestCase
                        //'viewTestCase': viewTestCase.is(':checked'),
                        //'createTestCase': createTestCase.is(':checked'),
                        //'editTestCase': editTestScript.is(':checked'),
                        //'deleteTestCase': deleteTestCase.is(':checked'),
                        // TestScript
                        'viewTestScript': viewTestScript.is(':checked'),
                        'createTestScript': createTestScript.is(':checked'),
                        'editTestScript': editTestScript.is(':checked'),
                        'deleteTestScript': deleteTestScript.is(':checked'),
                        // TestSuite
                        'viewTestSuite': viewTestSuite.is(':checked'),
                        'createTestSuite': createTestSuite.is(':checked'),
                        'editTestSuite': editTestSuite.is(':checked'),
                        'deleteTestSuite': deleteTestSuite.is(':checked'),
                        userProjectRoles: users
                    });
                });

                var Project = {
                    startDate: Date.now(), // current Date
                    //startDate: "2017-05-04T18:30:00Z",
                    projectName: $('#projectName').val().trim(),
                    tagsSet: tagsArray.join(', '),
                    applicationType: $('#cboAppType option:selected').text(), // Default : web
                    projectRoles: rolesArray
                };

                // call api 
                // Save in localStorage & redirect to page 2
                console.log('calling api');
                
                //
                var notice = new PNotify({
                    title: 'Saving Project',
                    text: "Please Wait",
                    type: 'info',
                    icon: 'fa fa-spinner fa-spin',
                    hide: false,
                    buttons: {
                        closer: false,
                        sticker: false
                    },
                    shadow: true,
                });

                // save & get the returned Obj & set in the localstorage                                 
                var saveTimer = setInterval(function () {
                    var returnObj = jqServices.createProject(Project); 
                    var nextPage = 'test_automation2.html';

                    console.log(returnObj);

                    if (returnObj !== undefined) {
                        clearInterval(saveTimer);
                        //
                        notice.update({
                            type: 'success',
                            icon:'fa fa-check',
                            title: 'Successfully created Project',
                            text: 'You will be redirected to the next page - "Create Test Suite"',
                            delay: 4000,
                            hide:true,
                            after_close: function (notice, timer_hide) {
                                console.log('notice :' + notice + ' timer_hide : ' + timer_hide);
                                if (nextPage) {
                                    location.href = nextPage;
                                }
                            }
                        });
                        //
                        localStorage.setItem('Project', JSON.stringify(returnObj));
                    }
                }, 800);
            }
        });
    }

    //TestAutomation - Create Test Suite - Page 2
    var createTestSuite = function () {
        console.log('TestSuite');

        // 1 get loggedin user
        // 2 get saved project id & tags from it
        // 3 form validation
        // 4  save - create json & call api

        var frmCreateTestSuite = $('#frmCreateTestSuite');
        var btnSave = $('#btnSaveTestSuite');
        var projectObj = JSON.parse(localStorage.getItem('Project'));

        // display tags
        if (projectObj !== undefined) {
            var Tags = $('#Tags');

            Tags.text(projectObj.tags.join(', '));
            //Tags.text(projectObj.tagsSet); 
        }

        //3 - form validation
        //frmCreateTestSuite.validate(); 


        //4 - save - create json & call api
        btnSave.on('click', function (e) {
            e.preventDefault();

            var txtTSuite = $('#tsName').val().trim();
            var txtModule = $('#moduleName').val().trim();
            var objTestSuites = [];

            if (txtTSuite.length && txtModule.length && projectObj !== undefined) {
                objTestSuites = [{
                    testSuiteName: txtTSuite,
                    moduleName: txtModule,
                    testScripts: [],
                    tags: projectObj.tags //  
                }];

                projectObj.testSuites = objTestSuites;

                // call api 
                // Save in localStorage & redirect to page 3
                console.log('calling api to save TestSuite');
                 
                // update project for Test Suite
                updateProject_ShowNotification({
                    projectObj: projectObj,
                    infoTitle: "Saving Test Suite",
                    infoText: "Please Wait",
                    successTitle: "Successfully created Test Suite",
                    title: "Test Suite",
                    nextPageTitle: 'Create Test Scripts',
                    nextPageUrl: 'test_automation3.html'
                });                 
            }
        });
    }


    // TestAutomation - createTestScripts - Page 3
    var createTestScript = function () {
        console.log('createTestScripts');

        // 1 get loggedin user
        // 2 get saved project 
        // 3 form validation
        // 4  save - create json & call api

        //
        var txtTestScriptName = $('#txtTestScriptName').val();
        var btnSave = $('#btnSave');
        var btnSave_Add = $('#btnSave_Add');
        var projectObj = JSON.parse(localStorage.getItem('Project'));

        // display tags
        if (projectObj !== undefined) {
            var Tags = $('#Tags');
            Tags.text(projectObj.tags.join(', '));
        }

        // display ModuleName
        if (projectObj !== undefined) {
            $('#lblTestSuiteName').text(projectObj.testSuites[0].testSuiteName);
            $('#txtModuleName').val(projectObj.testSuites[0].moduleName);
        }

        function generateTestStepsArray() {
            var stepsList = $('#Steps li');
            var testSteps = [];

            stepsList.each(function () {
                var cur = $(this);
                var stepNum = $('.stepNo span', cur).text();
                var stepDesc = $('.stepDesc input[type="text"]', cur).val().trim();
                var selectActionTypeVal = $('.selectActionType', cur).val();
                var action_Url = $('.action_Url input[type="text"]', cur).val();
                var action_fileUpload = $('.action_fileUpload', cur);
                var action_locator = $('.action_locator', cur);
                var action_locatorData = $('.action_locatorData', cur);
                var action_duration = $('.action_duration input[type="text"]', cur).val();

                var input1 = undefined,
                    input2 = undefined,
                    input3 = undefined;

                //console.log("input1 : " + input1);

                switch (selectActionTypeVal) {
                    case "Go to URL":                                                
                        input3 = action_Url;
                        break;

                    case "Take Screenshot":                        
                        input1 = $('.screenshot-location', action_fileUpload).val();
                        input2 = $('.file-name', action_fileUpload).val();
                        break;

                    case "Add Assertion":                        
                        input1 = $('.selectLocatorType', action_locator).val();
                        input2 = $('.locatorInputVal input[type="text"]', action_locator).val();
                        //console.log('Add Assertion input1 : ' + input1);
                        //console.log('Add Assertion input2 : ' + input2);
                        break;

                    case "API":                        
                        input3 = action_Url;
                        //console.log('Add API : ' + input3);
                        break;

                    case "Check":                        
                        input1 = $('.selectLocatorType', action_locator).val();
                        input2 = $('.locatorInputVal input[type="text"]', action_locator).val();
                        //console.log('Check input1 : ' + input1);
                        //console.log('Check input2 : ' + input2);
                        break;

                    case "Wait":
                        //$(this).closest("li").find(".action_duration").css("display", "flex");
                        input3 = action_duration;
                        //console.log('Wait : ' + input3);
                        break;

                    case "SendKeys":
                        //$(this).closest("li").find(".action_locatorData").css("display", "flex");
                        input1 = $('.selectLocatorType', action_locatorData).val();
                        input2 = $('.locatorInputVal input[type="text"]', action_locatorData).val();
                        input3 = $('.locatorInputVal:last-of-type input[type="text"]', action_locatorData).val();
                        //console.log(input1);
                        //console.log(input2);
                        //console.log(input3);

                        break;

                    case "Set":
                        //$(this).closest("li").find(".action_locatorData").css("display", "flex");
                        input1 = $('.selectLocatorType', action_locatorData).val();
                        input2 = $('.locatorInputVal input[type="text"]', action_locatorData).val();
                        input3 = $('.locatorInputVal:last-of-type input[type="text"]', action_locatorData).val();

                        //console.log(input1);
                        //console.log(input2);
                        //console.log(input3);
                        break;

                    case "Verify":
                        //$(this).closest("li").find(".action_locatorData").css("display", "flex");
                        input1 = $('.selectLocatorType', action_locatorData).val();
                        input2 = $('.locatorInputVal input[type="text"]', action_locatorData).val();
                        input3 = $('.locatorInputVal:last-of-type input[type="text"]', action_locatorData).val();

                        //console.log(input1);
                        //console.log(input2);
                        //console.log(input3);
                        break;

                    default:
                        //$(this).closest("li").find(".actionSelectedType").fadeOut();
                }

                testSteps.push({
                    "stepNum": stepNum,
                    "stepDescription": stepDesc,
                    "action": selectActionTypeVal,
                    "input1": input1,
                    "input2": input2,
                    "input3": input3
                });

            });

            console.log("testSteps : " + JSON.stringify(testSteps));
            return testSteps;
        }

        //save TestScript
        btnSave.on('click', function (e) {
            var testSteps = generateTestStepsArray(); ///

            // add new array
            var testScripts = [{
                "testScriptName": txtTestScriptName,
                "testSteps": testSteps
            }];

            if ($.isArray(projectObj.testSuites[0].testScripts)) {
                console.log('update testscripts Array');
                projectObj.testSuites[0].testScripts.push({
                    "testScriptName": txtTestScriptName,
                    "testSteps": testSteps
                });
            } else {
                console.log('create new array testScripts');
                projectObj.testSuites[0].testScripts = testScripts;
            }

            // update projectObj for testScripts
            //projectObj.testSuites[0].testScripts = testScripts;
            console.log(JSON.stringify(projectObj));

            //call api             
             //update project for Test Suite
            updateProject_ShowNotification({
                projectObj: projectObj,
                infoTitle: "Saving Test Scripts",
                infoText: "Please Wait",
                successTitle: "Successfully created Test Scripts",
                title: "Test Suite",
                nextPageTitle: 'Test Scripts',
                nextPageUrl: 'test_automation4.html'
            });
        });

        btnSave_Add.on('click', function (e) {

        });

    }

    // TestAutomation - Test Script - Page 4 
    var testScripts = function () {
        console.log('testScripts');

        var projectObj = JSON.parse(localStorage.getItem('Project'));
        var activeTestSuiteBreadcrumb = $('.breadcrumb li.active');
        var currentTestSuite = projectObj.testSuites[0];
        var testScripts = currentTestSuite.testScripts;
        var dummyProject = {
            testScripts: [
                { "id": 5, "testScriptName": "Ts1", "testSteps": [{ "id": 8, "stepNum": 1, "stepDescription": "Step 1", "action": "Go to URL", "input1": "url1", "input2": null, "input3": null }, { "id": 9, "stepNum": 2, "stepDescription": "Step 2", "action": "Take Screenshot", "input1": "sl1", "input2": "fn1", "input3": null }] }, { "id": 6, "testScriptName": "Ts2", "testSteps": [{ "id": 10, "stepNum": 1, "stepDescription": "step 3", "action": "Go to URL", "input1": "url2", "input2": null, "input3": null }, { "id": 11, "stepNum": 2, "stepDescription": "step 4", "action": "Take Screenshot", "input1": "sl2", "input2": "fn2", "input3": null }] }, { "id": 7, "testScriptName": "Ts3", "testSteps": [{ "id": 12, "stepNum": 1, "stepDescription": "s1", "action": "Add Assertion", "input1": "Id", "input2": "id1", "input3": null }, { "id": 13, "stepNum": 2, "stepDescription": "s2", "action": "API", "input1": null, "input2": null, "input3": "apiurl" }, { "id": 14, "stepNum": 3, "stepDescription": "s3", "action": "Wait", "input1": null, "input2": null, "input3": "300s" }, { "id": 15, "stepNum": 4, "stepDescription": "s4", "action": "", "input1": null, "input2": null, "input3": null }] }]
        };
        

        // display tags
        if (projectObj !== undefined) {
            var Tags = $('#Tags');
            Tags.text(projectObj.tags.join(', '));
            //Tags.text(projectObj.tagsSet); 
        }

        //
        activeTestSuiteBreadcrumb.text(currentTestSuite.testSuiteName);

        //load Template
        function loadTmplData() {
            var template = $('#testScript-tpl').html();
            Mustache.parse(template);   // optional, speeds up future uses
            //var data = {
            //    "testScripts": [
            //      { id: "1", "testScriptName": "dummy 1" },
            //      { id: "2", "testScriptName": "dummy 2" },
            //      { id: "3", "testScriptName": "dummy 3" }
            //    ]
            //};            
            var rendered = Mustache.render(template, dummyProject);
            $('#testScripts-block').html(rendered);
        }
        loadTmplData();

        function eventHandler() {
            var btnTsEdit = $('.btnTsEdit');
            var btnTsPlay = $('.btnTsPlay');
            var btnTsCopy = $('.btnTsCopy');
            var btnTsDelete = $('.btnTsDelete');

            //delete TestScript
            // remove form UI & objProject
            $(document).on('click', '.btnTsDelete', function (e) {
                var cur = $(this);
                var id = cur.data('testscript-id');
                var parentCard = cur.parentsUntil('.card').parent();

                $('#modal-deletetestscript').modal().one('click', '.delete', function (e) {
                    //projectObj.testSuites[0];
                    $.grep(dummyProject.testScripts, function (item, idx) {
                        //console.log(dummyProject.testScripts[0].testSteps.length);
                        console.log("item.id :" + JSON.stringify(item) + " id : " + id);
                        if (item && item.id === id) {
                            console.log('match');
                            var arr = dummyProject.testScripts.splice(idx, 1);
                            console.log(JSON.stringify(arr));
                            console.log(JSON.stringify(dummyProject.testScripts));
                            loadTmplData();
                            return;
                        }
                    });
                });

            });
             

            // delete test steps
            // remove form UI & objProject
            $(document).on('click', '.btnDelete-step', function (e) {
                var cur = $(this);
                var id = cur.data('stepid');
                var listItem = cur.closest('li');
                console.log(listItem);

                $('#modal-deleteSteps').modal().on('click', '.delete', function (e) {
                    //projectObj.testSuites[0];
                    console.log(listItem);
                    $.grep(dummyProject.testScripts, function (ts, idx) {
                        console.log(ts);
                        $.grep(ts.testSteps, function (step, idx) {
                            console.log(step);
                            if (step && step.id === id) {
                                console.log('match');
                                ts.testSteps.splice(idx, 1);
                                loadTmplData();
                                return;
                            }
                        });
                    });
                });
            });
            //$('.btnDelete-step').on('click', function (e) {
                
            //});
        }
        eventHandler();

        console.log('cur testScripts : ' + JSON.stringify(testScripts));
        console.log("testScripts :" + testScripts.length);
    }


    // TestAutomation - helper method
    var updateProject_ShowNotification = function (opts) {
        //delay = delay || 4000;
        opts.infoTitle = opts.infoTitle || "Saving";
        opts.infoText = opts.infoText || "Please Wait";
        opts.successTitle = opts.successTitle || 'Successfully created ' + title;
        opts.successText = opts.successText || "You will be redirected to the next page " + opts.nextPageTitle;

        var notice = new PNotify({
            title: opts.infoTitle,
            text: opts.infoText,
            type: 'info', ///////
            icon: 'fa fa-spinner fa-spin',
            hide: false,
            buttons: {
                closer: false,
                sticker: false
            },
            shadow: true,
        });

        // save & get the returned Obj & set in the localstorage                                 
        var saveTimer = setInterval(function () {
            var returnObj = jqServices.updateProject(opts.projectObj);
            //console.log(returnObj);

            if (returnObj !== undefined) {
                clearInterval(saveTimer);
                //
                notice.update({
                    type: 'success',
                    icon: 'fa fa-check',
                    title: opts.successTitle,
                    text: opts.successText,
                    delay: 3000,
                    hide: true,
                    after_close: function (notice, timer_hide) {                        
                        if (opts.nextPageUrl) {
                            location.href = opts.nextPageUrl;
                        }
                    }
                });
                //
                localStorage.setItem('Project', JSON.stringify(returnObj));
            }
        }, 800);
    }

    // init - testAutomation
    var init = function (page) {        
        //
        switch (page) {
            case "createProject":
                createProject();
                break;
            case "createTestSuite":
                createTestSuite();
                break;
            case "createTestScript":
                createTestScript();
                break;
            case "testScripts":
                testScripts();
                break;
        }         
    }
    return { init: init }
}(jqServices));

//////////////////////////////////////////////////////////////////////////////////

(function () {    
    testAutomation.init($('body').data('page'));
    
}());

