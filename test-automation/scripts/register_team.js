
           $(document).ready(function () {

               // Handler for .ready() called.
               $(".submit").click(function(){
                   $(".alert-success").fadeIn();
                   //$("html, body").animate({ scrollTop: 0 }, 600);

                   window.setTimeout(function () {
                       location.href = "signin.html";
                   }, 5000);
               });
           });
       /***================password strength===============**/
           $(document).ready(function() {

               $("#example-progress-bar").focus(function(){
                   $("#example-progress-bar-container").css("display", "block");
               });
               $("#example-progress-bar").focusout(function(){
                   $("#example-progress-bar-container").css("display", "none");
               });
               $('#example-progress-bar').strengthMeter('progressBar', {
                   container: $('#example-progress-bar-container')
               });


           });
        /***===============password strength==============**/
       /***===================placeholder==============**/
           $('.form-control').on('focus blur', function (e) {
               $(this).parents('.form-group').toggleClass('focused', (e.type === 'focus' || this.value.length > 0));
               $(this).removeAttr("placeholder");
           }).trigger('blur');
       /***================placeholder===============**/

           $(function() {
               $('#cp2').colorpicker();
           });
           $(function() {
               $('#cp3').colorpicker();
           });

         function checkWidth()
         {
             /*If browser resized, check width again */
             if ($(window).width() <= 480) {
                 $('#main-registration-div').addClass('bg-body');
                 $('body').removeClass('bg-body');
             }
             else {
                 if ($(window).width() > 480) {
                     $('body').addClass('bg-body');
                     $('#main-login-div').removeClass('bg-body');
                 }
             }
         }
         $(document).ready(function() {
           checkWidth();
           adjustSignInBtn();
       });

        /*****putting signin button above for responsive***/
         function adjustSignInBtn() {
           var windowWidth = $(window).width();

           if(windowWidth <=767) {
             $( ".signinBtnDiv" ).insertBefore( ".formTitle" );
           }
           else
           {
             $( ".signinBtnDiv" ).insertAfter( ".formTitle" );
           }
         }
