

$('[name="requirement_option"]').change(function() {
	var reqOptionval = $("input[name='requirement_option']:checked").val();
	console.log("reqOptionval"+ reqOptionval);
	if(reqOptionval === "importReq") {
		$(".formGrpLeft").fadeIn();
	}
  else
  {
    $(".formGrpLeft").fadeOut();
  }
});

$(".btnTestConnSucess").click(function() {
	$(".successMsg").fadeIn(function() {
		$(".formGrpRight").fadeIn();
	});
})

/***add cycle**/

$(".btnAddCycle").click(function() {

	var cloneElem = $(this).parent().find(".rCycle:first").clone();

	var releaseNo = $("#configCycles .tabBody .rCycle").length;

	var nextreleaseNo = releaseNo + 1;
  cloneElem.attr("id","rCycle"+nextreleaseNo);
  cloneElem.find(".rCycleNo").val(nextreleaseNo);

  cloneElem.find(".form-control").val("");
  cloneElem.find(".md-input").val("");
  cloneElem.find(".summernote").code('');
  
  cloneElem.find(".rCycleNo").val(nextreleaseNo);
	cloneElem.prepend("<hr>");
	//cloneElem.find(".rNo span").html(nextreleaseNo);

	$("#configCycles .tabBody").append(cloneElem);

});


$(document).on("click",".startDatePicker",function() {
  $(this).datetimepicker();
});
$(document).on("click",".endDatePicker",function() {
  $(this).datetimepicker();
});

/**activate formbuilder*****/

/*$(".btnAddCustomField").click(function(){
  $(this).parents(".tabBody").find(".customField-editor").slideDown();
  $('html, body').animate({
        scrollTop: $(this).parents(".tabBody").find(".customField-editor").offset().top
  },1000);
});*/


$(document).on("click",".btnAddTsSteps",function() {
  var stepTableTr = $(".testCaseSteps > .table > tbody > tr:last").clone();
  $(".testCaseSteps > .table > tbody").append(stepTableTr);
});



/***add member******/
$(document).on("click",".userSelectBtn",function() {
  $(".card").css("z-index",21);
  $(this).parents(".card").css("z-index",25);
  $(this).parents(".addMemberGrp").toggleClass("active");
  $(this).parent(".addMemberGrp").find("input[type=checkbox]").attr('checked', false).removeClass("memActive");
});



$(document).on("click",".addMember",function() {
  $(this).parents(".addMemberGrp").toggleClass("active");


  var memLength = $(this).parents(".addUserContent").find(".memberAddedList > ul > li").length;
  $('.addMemberGrp input[type="checkbox"]').each(function() {

      if ($(this).hasClass('memActive')) {
       var activeMemName =  $(this).parent().find("span").html();
       var memberLi = '<li class="col-lg-2 col-sm-4 col-xs-6"><div class="card"><span><i class="ion-ios-person"></i></span><div class="card-block"><h6 class="card-title">'+activeMemName+'</h6><p style="display: block; height: 37px; padding-top: 6.4px; margin-top: 0px; padding-bottom: 3.2px; margin-bottom: 0px; overflow: hidden;"><a href="javascript:;" class="btn-del pull-left"><i class="ion-edit"></i></a><a href="javascript:;" class="btn-del pull-right" data-toggle="modal" data-target="#modal-deletemember"><i class="ion-ios-trash-outline"></i></a></p></div></div></li>';
      $(".addMember").parents(".addUserContent").find(".memberAddedList > ul").append(memberLi);
      memLength=memLength+ 1;
      $(".addMember").parents(".addUserHeader").find("p span").html(memLength);
      
      }
  })

   
});



$('.addMemberGrp input[type="checkbox"]').change(function() {
  if ($(this).is(':checked')) {
    $(this).addClass("memActive");
  }
})








$(".date .form-control").click(function() {
  $(this).parent().find(".input-group-addon").click();
});

$(document).on({
    mouseenter: function () {
        $(this).find(".card-block p").stop().slideDown(300);
    },
    mouseleave: function () {
        $(this).find(".card-block p").stop().slideUp(300);
    }
}, ".memberAddedList > ul > li .card");



/***show execute screen**/

function showNextEscreen(num) {
	$(".eScreen").hide();
	$("#eScreen"+num).show();
}
















/***workspace******/

var wbox1Height = $(".wbox1 .box-body").height();
$(".wbox2 .box-body").height(wbox1Height);

var wbox1Height = $(".wbox4 .box-body").height();
$(".wbox3 .box-body").height(wbox1Height);






/**design****/

var liTreeNodeIndex = $(".ul1 > li").length;
var nextliTreeNodeIndex = 0;
function showNextTab(num) {
  if(num < 5) {

    if(num===3) {
      var instance = $('#releaseForm').parsley();
      instance.validate();
      if(!instance.isValid()) {
        return false;
      }
      else
      {
        $("#aconfig"+num).unbind().click();
        var releaseName = $(".releaseName").val();
        $("#configCycles").find(".rName > span").html(releaseName);
        if(releaseName != "") {
          
          nextliTreeNodeIndex = liTreeNodeIndex + 1;
          var liTreeNode = '<li id="li'+nextliTreeNodeIndex+'"><a href="javascript:;" class="nav-link active" data-toggle="tab" role="tab"><span>'+releaseName+'</span></a></li>';
          $(".ul1").append(liTreeNode);
        }
      }
    }
    else if(num===4) {
      var instance = $('#cycleForm').parsley();
      instance.validate();
      if(!instance.isValid()) {
        return false;
      }
      else
      {
        $("#aconfig"+num).unbind().click();
        $('<ul/>', {
              class: 'ul2'
          }).appendTo('.ul1 > li#li'+nextliTreeNodeIndex);
        var rCycleCounter = 0;
        $(".rCycle").each(function() {
          
          var cycleName = $(this).find(".cycleName").val();
          if(cycleName != "") {
            var liTreeNode = '<li><a href="#tab'+cycleName+'" data-target="tab'+cycleName+'" class="nav-link a_ts active" data-toggle="tab" role="tab"><span>'+cycleName+'</span></a></li>';
            $(".ul2").append(liTreeNode);

            var cloneElem = $(".tsContent #tsRef").clone();
           

            cloneElem.attr("id","tab"+cycleName);
             $(".tsContent").append(cloneElem);


             if(rCycleCounter ===0) {
              $('a[data-target="tab'+cycleName+'"]').addClass("tsActive");
              $("#tab"+cycleName).show();
             }
             rCycleCounter ++;

          }
        });

        var releaseName = $(".releaseName").val();
        $("#configCycles").find(".rName > span").html(releaseName);


      }
    }
    else
    {
      $("#aconfig"+num).unbind().click();
    }
    
  }
  else
  {
    var instance = $('#testCaseForm').parsley();
      instance.validate();
      if(!instance.isValid()) {
        return false;
      }
      else
      {
        showTsAlert(5);
      }
    
  }
  
}


/****tsmenu a tag****/

$(document).on("click",".a_ts",function() {
  var tabId = $(this).attr("data-target");
  $(".tab-panel").hide();
  $("#"+tabId).fadeIn();
  $(".a_ts").removeClass("tsActive");
  $(this).addClass("tsActive");
});

$(document).on("click",".ul3 > li > a",function() {
  
  $(".ul3 > li > a").removeClass("tsActiveInner");
  $(this).addClass("tsActiveInner");
});



/***tsmenu a tag click ends******/

$(document).on("click",".btnSaveAdd",function() {
  var instance = $(this).parents('#testCaseForm').parsley();
  instance.validate();
  if(!instance.isValid()) {
    return false;
  }
  else
  {

    var testSuiteName = $(this).parents('.tab-panel').find(".testsuiteName").val();
    var testCaseName = $(this).parents('.tab-panel').find(".testCaseName").val();
    var appendToNode = $('.ul3 a[data-target="'+testSuiteName+'"]').parent();
    if(testCaseName.trim() != "") {
      if($('.ul3 a[data-target="'+testSuiteName+'"]').parent().find(".ul4").length === 0)
        $('<ul/>', {
              class: 'ul4'
          }).appendTo(appendToNode);
     
      
      var liTreeNode = '<li><a href="#tstab'+testCaseName+'" data-target="tstab'+testCaseName+'" class="nav-link active" data-toggle="tab" role="tab"><span>'+testCaseName+'</span></a></li>';
            $('.ul3 a[data-target="'+testSuiteName+'"]').parent().find('.ul4').append(liTreeNode);
    }

    $(this).parents(".tsDiv").find(".form-control").val("");
    $(this).parents(".tsDiv").find(".md-input").val("");
    $(this).parents(".tsDiv").find(".summernote").code('');

    $('html, body').animate({
        scrollTop: $(this).parents("#testCaseDiv").offset().top
    },1000);
    

  }
});



/***tsmenu a tag click ends******/

$(document).on("click",".saveTc",function() {
  var instance = $(this).parents('#testCaseForm').parsley();
  instance.validate();
  if(!instance.isValid()) {
    return false;
  }
  else
  {

    var testSuiteName = $(this).parents('.tab-panel').find(".testsuiteName").val();
    var testCaseName = $(this).parents('.tab-panel').find(".testCaseName").val();
    var appendToNode = $('.ul3 a[data-target="'+testSuiteName+'"]').parent();
    if(testCaseName.trim() != "") {
      if($('.ul3 a[data-target="'+testSuiteName+'"]').parent().find(".ul4").length === 0)
        $('<ul/>', {
              class: 'ul4'
          }).appendTo(appendToNode);
     
      
      var liTreeNode = '<li><a href="#tstab'+testCaseName+'" data-target="tstab'+testCaseName+'" class="nav-link active" data-toggle="tab" role="tab"><span>'+testCaseName+'</span></a></li>';
            $('.ul3 a[data-target="'+testSuiteName+'"]').parent().find('.ul4').append(liTreeNode);
    }

   showTsAlert(5);
    

  }
});



/**clear**/
$(document).on("click",".btnClear",function(){
  $(this).parents(".tab-pane").find(".form-control").val("");
  $(this).parents(".tab-pane").find(".md-input").val("");
  $(this).parents(".tab-pane").find(".summernote").code('');
})


$(document).on("click","#generatedTags2 .tagLabel",function() {
  $(this).toggleClass("tagLabelActive");
});



$(document).on("click",".btnToTestSuite",function() {
  var instance = $(this).parents('#testSuiteForm').parsley();
  instance.validate();
  if(!instance.isValid()) {
    return false;
  }
  else
  {
    var testsuiteName = $(this).parents('#testSuiteForm').find(".testsuiteName").val();
    var currentTabId = $(this).parents('.tab-panel').attr("id");
    var appendToNode = $('.ul2 a[data-target="'+currentTabId+'"]').parent();
    if(testsuiteName.trim() != "") {
      $('<ul/>', {
              class: 'ul3'
          }).appendTo(appendToNode);
      var liTreeNode = '<li><a href="javascript:;" class="nav-link active" data-toggle="tab" data-target="'+testsuiteName+'" role="tab"><span>'+testsuiteName+'</span></a></li>';
            $('.ul2 a[data-target="'+currentTabId+'"]').parent().find(".ul3").append(liTreeNode);

    }
    var moduleName = $(this).parents('#testSuiteForm').find(".moduleName").val();
    if(moduleName.trim() != "") {
      $(this).parents(".tab-panel").find(".tsDiv .moduleNameText > span").html(moduleName);
    }
    $(this).parents(".tab-panel").find(".tsDiv").hide();
    $(this).parents(".tab-panel").find("#testCaseDiv").fadeIn();
  }
})







function showTsAlert(num) {
  var alertDiv = document.createElement('div');
  var alertDivContent = "";
  var pglocation = "";
  $(alertDiv).addClass("alert").addClass("alert-success");
  $(alertDiv).attr("role", "alert");

  switch(num) {
    case 5:
    alertDivContent = "Design Configured successfully";
    pglocation = "step_execute.html";
    break;
    case 10:
    alertDivContent = "Plan excecution completed";
    pglocation = "step_execute.html";
    break;
   default:
    $(alertDiv).hide();
  }

  $(alertDiv).append("<p>"+alertDivContent+"</p>");
  
  $(".page-header").prepend($(alertDiv));
  $(alertDiv).fadeIn();


  window.setTimeout(function () {
    $(alertDiv).fadeOut().remove();
    location.href = pglocation;
  }, 4000);
}




$(document).on("click",".peBtn > a",function() {
  
  $(this).parents("td").find("a").css("color","#ddd");
  $(this).addClass("pe_activeBtn");
})

/*$(".btnSaveAdd").click(function() {
  $(this).parents("#configTestSuites").find("input").val("");
  $(this).parents("#configTestSuites").find("select").val("");
})*/


 $('.configTemplateNavTabs a').bind('click mousedown dblclick',function(e){
       e.preventDefault()
       e.stopImmediatePropagation()
});