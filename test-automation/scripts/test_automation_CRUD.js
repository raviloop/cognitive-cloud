////////////////////////// Services //////////////////////////
var jqServices = (function () {
    'use strict';
    // alert('lia')
    var access_token = localStorage.accessToken; /////////
    var selectedTeamId = localStorage.selectedTeamId;
    var UAA_MICROSERVICE_ENDPOINT = "http://52.40.247.241:30513";
    var USER_MGMT_MICROSERICE_ENDPOINT = "http://52.40.247.241:30514";
    var TEST_AUTOMATION_ENDPOINT = "http://52.40.247.241:30515";
    var TEST_AUTOMATION_RUNNER = "http://35.164.197.23:8761/runSelJar";

    // console.log(access_token);
    //
    var getAllUsers = function () {
        var jqXHR = $.ajax({
            method: "GET",
            url: USER_MGMT_MICROSERICE_ENDPOINT + '/api/teammates/' + selectedTeamId + '?access_token=' + access_token,
            data: '',
            //dataType: "json",
            cache: false,
            crossDomain: false, // default: false
            headers: {
                Authorization: 'Bearer ' + access_token
            },
            success: function (data, status, jqxhr) {
                // create users list and prepend
                var subNavBtn = $('#memberList-wrapper ul');
                var str = '';
                $.grep(data, function (user) {
                    var userid = user.login;
                    var fullName = user.firstName + ' ' + user.lastName;
                    var userEmail = user.login;

                    str += '<li class="list-group-item">';
                    str += '<div class="checkbox-custom checkbox-info margin-0">';
                    str += '<input id="' + userid + '" type="checkbox" value="' + fullName + '" data-fullName="' + fullName + '" data-userid="' + userid + '">';
                    str += '<label for="' + userid + '" userEmail="' + userEmail + ' ">' + fullName + '</label>';

                });
                //str += '</ul>';

                //$(str).insertBefore(subNavBtn);
                $(subNavBtn).html(str);
                console.log('success');
            },
            complete: function (jqxhr, status) {
                //console.log('status : ' + status);
            },
            error: function (jqxhr, status, errorThrown) {
                console.log('Error while getting all users : ' + status + " : " + errorThrown);
            }
        });
    };
    var getUser = function (callback) {
        var returnData = undefined;
        var jqXHR = $.ajax({
            method: "GET",
            url: UAA_MICROSERVICE_ENDPOINT + '/api/account',
            data: '',
            dataType: "json",
            cache: false,
            crossDomain: false, // default: false
            headers: {
                'Authorization': 'Bearer ' + access_token,
                'Content-Type': 'application/json'
            },
            success: function (data, status, jqxhr) {
                //console.log('success - getUser : ' + JSON.stringify(data));
                callback(data);
            },
            error: function (jqxhr, status, errorThrown) {
                console.log('Error getUser : ' + status + " : " + errorThrown);
            }
        });
        return returnData;
    }

    //Get all projects on landing screen
    var getAllProjects = function (callback) {
        var jqXHR = $.ajax({
            method: "GET",
            url: TEST_AUTOMATION_ENDPOINT + '/api/projects?access_token=' + access_token,
            data: '',
            dataType: "json",
            cache: false,
            crossDomain: false, // default: false
            headers: {
                'Authorization': 'Bearer ' + access_token,
                'Content-Type': 'application/json'
            },
            success: function (data, status, jqxhr) {
               /* console.log('success: ' + JSON.stringify(data));
                console.log(data);*/
                callback(data);
            },
            error: function (jqxhr, status, errorThrown) {
                console.log('Error getUser : ' + status + " : " + errorThrown);
            }
        });

    }


    //createProject
    var createProject = function (objProject) {
        var jsonObject = "";

        objProject.teamId = selectedTeamId;
        var returnObj = undefined;
        var jqXHR = $.ajax({
            method: "POST",
            url: TEST_AUTOMATION_ENDPOINT + '/api/projects?access_token=' + access_token, ////////
            data: JSON.stringify(objProject), /////
            dataType: "json", ////
            cache: false,
            async: false, ///// default : true
            crossDomain: false, // default: false
            headers: {
                Authorization: 'Bearer ' + access_token,
                'Content-Type': "application/json" ///////////
            },
            success: function (data, status, jqxhr) {
                // save in localStorage
                console.log('Success - Create Project: ' + JSON.stringify(data));
                //localStorage.setItem('Project', data);
                returnObj = data;
            },
            error: function (jqxhr, status, errorThrown) {
                console.log('Error - Create Project : ' + status + " : " + errorThrown);
            }
        });

        //jqXHR.success(function (data) {
        //    console.log('success 2 ==== ' + data);
        //    returnObj = data;
        //});

        return returnObj;

    };

    // updateProject
    var updateProject = function (objProject) {
        objProject.teamId = selectedTeamId;
        var returnObj = undefined;
        var jqXHR = $.ajax({
            method: "PUT",
            url: TEST_AUTOMATION_ENDPOINT + '/api/projects?access_token=' + access_token, ////////
            data: JSON.stringify(objProject), /////
            dataType: "json", ////
            cache: false,
            async: false, ///// default : true
            crossDomain: false, // default: false
            headers: {
                Authorization: 'Bearer ' + access_token,
                'Content-Type': "application/json" ///////////
            },
            success: function (data, status, jqxhr) {
                console.log('Update sucessfull for: ' + objProject.teamId);
                returnObj = data;
            },
            error: function (jqxhr, status, errorThrown) {
                console.log('Error - createTestSuite : ' + status + " : " + errorThrown);
            }
        });
        return returnObj;
    };

    // createTestScripts
    var createTestScript = function (testScriptId) {
        var returnObj = undefined;
        var jqXHR = $.ajax({
            method: "GET",
            url: TEST_AUTOMATION_ENDPOINT + '/api/create-test-scripts/' + testScriptId + '?access_token=' + access_token, ////////
            cache: false,
            async: false, ///// default : true
            crossDomain: false, // default: false
            headers: {
                Authorization: 'Bearer ' + access_token,
                'Content-Type': "application/json"
            },
            success: function (data, status, jqxhr) {
                console.log('success - createTestScript :' + data);
            },
            error: function (jqxhr, status, errorThrown) {
                console.log('Error - createTestScripts : ' + status + " : " + errorThrown);
            }
        });
        return returnObj;
    };

    // runTestScript
    var runTestScript = function (testScriptId) {
        var returnObj = undefined;
        var jqXHR = $.ajax({
            method: "GET",
            //url: TEST_AUTOMATION_RUNNER + '/api/run-test-script/'+ testScriptId + '?access_token=' + access_token, /////////////
            url: TEST_AUTOMATION_RUNNER, /////////////
            cache: false,
            async: false, ///// default : true
            crossDomain: true, // default: false
            headers: {
                Authorization: 'Bearer ' + access_token,
                //'Content-Type': "application/json"  ///////////
            },
            success: function (data, status, jqxhr) {
                console.log('success - runTestScript :' + data);
                returnObj = data;
            },
            error: function (jqxhr, status, errorThrown) {
                console.log('Error - runTestScript : ' + status + " : " + errorThrown);
            }
        });
        return returnObj;
    };

    // createTestScripts
    var runTestResults = function (testScriptId) {
        console.log('inside run call');
        var returnObj = undefined;
        var jqXHR = $.ajax({
            method: "GET",
            url: 'http://52.40.247.241:30515/api-public/test-execution-status?testScriptId='+testScriptId, ////////
            cache: false,
            async: false, ///// default : true
            crossDomain: false, // default: false
            headers: {
                contentType: 'application/json'
            },
            success: function (data, status, jqxhr) {
                returnObj = data.executionStatus;
                if(data.executionStatus != undefined)
                {
                    $('.resultSteps').find('#pearl_'+data.executionStatus).prevAll('.pearl').andSelf().addClass('done current');
                }

                var timer =  setTimeout(function() {
                    if(data.executionStatus != "COMPLETED")
                    {   
                        if(data.executionStatus == undefined)
                        {   

                            runTestResults(testScriptId);
                        }
                        else{ 
                            runTestResults(data.testScriptID);
                        }
                    }
                    else if(data.executionStatus == "COMPLETED"){
                        stopCalling();
                    }
                },2000);
                

                function stopCalling(){
                    clearTimeout(timer); 
                }
            },
            error: function (jqxhr, status, errorThrown) {
                console.log('Error - createTestScripts : ' + status + " : " + errorThrown);
            }
        });
        return returnObj;
    };
    
    
    return {
        getAllUsers: getAllUsers,
        getUser: getUser,
        getAllProjects: getAllProjects,
        createProject: createProject,
        updateProject: updateProject,
        createTestScript: createTestScript,
        runTestScript: runTestScript,
        runTestResults: runTestResults
    }
}());

/////////////////////////// Test Automation //////////////////

