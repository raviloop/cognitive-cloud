

$("#sevicePanel1 .card-footer .btn").click(function() {
  $(".sevicePanel").fadeOut(function() {
    $("#sevicePanel2").show();
  });
});

$("#sevicePanel2 .card-footer .btn").click(function() {
  $(".sevicePanel").fadeOut(function() {
    $("#sevicePanel3").show();
  });
});


$(document).ready(function() {
	var windowHeight = $(window).height();
	var panelHeadingHeight = $(".panel_serviceStore .panel-heading").height();
	var panelbtntoggle = $(".panel_serviceStore .panel-heading #btn-toggle").outerHeight();
	var panel_serviceStoreTop = panelHeadingHeight+panelbtntoggle;
	$(".panel_serviceStore").height(panelHeadingHeight);

	var headerHeight = 60;
	var heightToExtend = (windowHeight -headerHeight) - panelbtntoggle;

	var panel_serviceStoreFlag = 0;
	$(".panel-heading #btn-toggle").click(function() {
		if(panel_serviceStoreFlag === 0) {
			showServiceStore();
		}
		else
		{
			$(".panel_serviceStore").height(panelHeadingHeight);
			panel_serviceStoreFlag = 0;
			$(this).find("i").removeClass("ion-chevron-down");
			$(this).find("i").addClass("ion-chevron-up");
			$("html,body").css("overflow","auto");
		}
		
	});

	$(".panel-heading .form-control").keyup(function() {
		var searchLength = $(this).val().length;
		if(searchLength != 0) {
			showServiceStore();
		}
		
		
	});

	$(".userSerivce .blankCard").click(function() {
		showServiceStore();
	})

function showServiceStore() {
	$(".panel_serviceStore").height(heightToExtend);
	$(".panel_serviceStore #btn-toggle").find("i").removeClass("ssArrowAnim");
	$(".panel_serviceStore #btn-toggle").find("i").removeClass("ion-chevron-up");
	$(".panel_serviceStore #btn-toggle").find("i").addClass("ion-chevron-down");

	$("html,body").css("overflow","hidden");
	panel_serviceStoreFlag = 1;
}	
});




/***add services*****/

function addService(serviceName) {
	$("#purchase_"+serviceName).find(".glyphicons").addClass("icon-file-check");
	$("#purchase_"+serviceName).find(".servicetext").html("Purchased").css("color","green");
	$("#purchase_"+serviceName).find(".serviceAction").html("Expires on - Dec 31, 2017").css("color","#fe0000");
	
	var stitle = $("#purchase_"+serviceName).parent().find("h5 b a").html();
	var serviceBox = $(".teamSerices > ul > li:first-child").clone();

	serviceBox.find(".card-block").html("");
	$("#purchase_"+serviceName).parent().find(".sText p").each(function() {
		var features = $(this).find("a").html();
		serviceBox.find(".card-block").append("<p>"+features+"</p>");

	})
	serviceBox.find(".card-header h6").html(stitle);
	serviceBox.insertAfter(".teamSerices > ul > li:first-child");

	if(serviceName === "ta") {
		serviceBox.find(".card-footer a").attr("href", "test_automation.html");
	}
	else if(serviceName === "tm") {
		serviceBox.find(".card-footer a").attr("href", "setup-roles.html");
		
	}



}


var $clearfix = '<div class="clearfix"></div>';
$($clearfix).insertAfter(".servicesRow .col-md-4:nth-child(3n)");


