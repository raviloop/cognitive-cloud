// $('#testAutomationProject').
var projects;
var pageStatus;
var objProject;
var testSuites = [];
var testSuite = {};
var select_project;
var backS2click = false;
var backS3click = false;

$(document).ready(function () {

   /* alert('sfd')

   alert(localStorage.selectedTeamId);*/

   var listItems = '';
   jqServices.getAllProjects(function (data) {
        // alert(data);
        console.log(data);
        projects = data;
        for (var i = 0; i < data.length; i++) {
            //console.log(data[i].projectName);
            listItems += "<option value='" + data[i].id + "'>" + data[i].projectName + "</option>";
        }
        $("#testAutomationProject").append(listItems);
      /*  $("#testAutomationProject").html(listItems).change(function (e) {
            var select_project = $.grep(projects, function (v) {
                return v.id == $("#testAutomationProject").val();
            })[0];*/
            /*var str = '';
            // console.log(JSON.stringify(select_project))
            $.each(select_project.testSuites, function (i, item) {

                //pageCounter('existingProject');
                pageStatus = 'existingProject';
                str += '<div class="panel"><div class="panel-heading padding-20"><span>' + item.testSuiteName + '</span></div></div>';
            });
            $('#pg_ExistingTestSuite').html(str);*/

        /*    $("#pg_ExistingTestSuite").load('template.html #registrantMembers', function () {
                var options = {
                    templateId: '#registrantMembers',
                    msgContainerId: '#pg_ExistingTestSuite'
                };

                var msg = {};
                msg.tsObject = select_project.testSuites;
                var template = $(options.templateId).html();
                var msgContainer = $(options.msgContainerId);
                Mustache.parse(template);
                var rendered = Mustache.render(template, msg);
                msgContainer.html(rendered);

            });

        });*/
    });



   $('.btnShowList').on('click', function (e) {
    $(this).closest(".memberList").find("")
    $('.memberListing').toggleClass('slide');
});


   $('.btnAddMember').on('click', function (e) {
    var arr = [];
    var eleParent = $(this).closest(".memberListing");

    eleParent.find('input:checked').each(function () {
        var labeltxt = $(this).siblings('label').text();
        var parent = $(this).parents('.list-group-item');
        parent.remove();
        var obj = {};
        obj.name = labeltxt;
        obj.id = $(this).attr("data-userid");
        arr.push(obj);
    });

    var blockDiv = $(this).closest(".addUserBlock");
    var str = '';
    $.grep(arr, function (k, v) {

        str += '<li class="col-sm-4 col-xs-6">';
        str += '<div class="card text-center">';
        str += '<i class="icon ion-ios-person personIcon" aria-hidden="true"></i>';
        str += '<span class="personName" userEmail="' + k.id + '">' + k.name + '</span>'
        str += '<p class="memOptions"><a href="javascript:;" class="btn-del pull-left"><i class="icon ion-edit" aria-hidden="true"></i></a>' +
        '<a href="javascript:;" class="btn-del pull-right" data-toggle="modal" data-target="#modal-deleteSteps">' +
        '<i class="ion-ios-trash-outline"></i></a></p></div></li>';

    });
    blockDiv.find('#memberListGroup').html(blockDiv.find('#memberListGroup').html() + '' + str);
    eleParent.toggleClass('slide');
});


   $('.memberListGroup').on('click', '.btn-del', function (e) {
    e.preventDefault();
    var parentLi = $(this).closest('li');
    var mem = parentLi.find('.personName').text();

    var blockDiv = $(this).closest(".addUserBlock");
    var memberLength = blockDiv.find('.memberListing li').length;



    //    var clone = blockDiv.find('.memberListing li:last-child').clone();
    //    clone.find('label').text(mem);
    //    clone.find('label').attr("for","inputChecked"+(memberLength+10));
    //    clone.find('input[type="checkbox"]').attr("id","inputChecked"+(memberLength+10));
    //    clone.appendTo(blockDiv.find('.memberListing ul'));

    var memberLi = '<li class="list-group-item"><div class="checkbox-custom checkbox-info margin-0"><input type="checkbox" id="inputChecked' + (memberLength + 10) + '"><label for="inputChecked' + (memberLength + 10) + '">' + mem + '</label></div></li>';
    blockDiv.find('.memberListing ul').append(memberLi);


    parentLi.remove();
    //console.log(mem);
});





   var invokerForPlay;
   var tsId;
   $('#runTSModal').on('show.bs.modal', function (e) {
    invokerForPlay = $(e.relatedTarget);
    // var tsIdTem =  invokerForPlay.closest('.panel').find(".testSuiteId").val();
    var tsNameTem =  invokerForPlay.closest('.panel').find(".modName").next().text();
    console.log(tsNameTem);

    var obj = objProject.testSuites;   
    var ts_i;
    obj.some(function(entry, i) {
        if (entry.testSuiteName == tsNameTem.trim()) {
            ts_i = i;
            return true;
        }
    });

    tsId = objProject.testSuites[ts_i].id;
   // alert(tsId);

});



   var invokerForSteps;
   $('#modalDeleteSteps').on('show.bs.modal', function (e) {
    invokerForSteps = $(e.relatedTarget);
});
   $('#btndeleteStepConfirm').on("click", function () {
    invokerForSteps.closest(".panel").remove();
    swal({
        title: "Confirmed",
        text: "Step deleted successfully",
        type: "success",
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Done',
        timer: 3000
    });

    $("#pg_Steps").find(".panel:first-child .panel-action").hide();
    var stepCounter = 1;
    $("#pg_Steps").find(".panel").each(function () {
        $(this).find("#stepNo").html(stepCounter);
        stepCounter++;
    });

});

   var invokerForTestScript;
   $('#modalDeleteTestScript').on('show.bs.modal', function (e) {
    invokerForTestScript = $(e.relatedTarget);
});
   $('#btndeleteTestScriptConfirm').on("click", function () {
    invokerForTestScript.closest(".panel").remove();
    swal({
        title: "Confirmed",
        text: "Test Script deleted successfully",
        type: "success",
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Done',
        timer: 3000
    });
});

   var invokerForTestSuite;
   $('#modalDeleteTestSuite').on('show.bs.modal', function (e) {
    invokerForTestSuite = $(e.relatedTarget);
});
   $('#btndeleteTestSuiteConfirm').on("click", function () {
    invokerForTestSuite.closest(".panel").remove();
    swal({
        title: "Confirmed",
        text: "Test Suite deleted successfully",
        type: "success",
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Done',
        timer: 3000
    });
});

   var tagLabels = "";
   /***show app div on select mobile***/
   $(".selectAppType").change(function () {
    var selectAppTypeVal = $(this).val();
    if (selectAppTypeVal === "Mobile") {
        $(".selectOsDiv").fadeIn();
    } else {
        $(".selectOsDiv").fadeOut();
    }
});




   /*****20170904 */
   $(document).on("click", ".btnRoleAction", function () {
    var eleParent = $(this).closest("tr");
    eleParent.find("input").attr("disabled", false);
});

   $(document).on("change", ".roleCheckBox input", function () {
    if ($(this).prop("checked") == true) {
        $(this).parent().find("i").addClass("rActive");
        $(this).parent().find("i").css("color", "rgb(34, 182, 110)");
    } else {
        $(this).parent().find("i").removeClass("rActive");
        $(this).parent().find("i").css("color", "rgb(239, 25, 60)");
    }
});

   $("#startNewProject").click(function () {

         jqServices.getAllUsers();
    $(".secSelectProject").hide(function () {
        $(".newProject").fadeIn();
        //pageCounter('CreateProject');
        pageStatus = 'CreateProject';
    });
});

   $(document).on("change", "#testAutomationProject", function () {
    // $(".wizard-buttons > a[data-wizard='next']").click();
    select_project = $.grep(projects, function (v) {
        return v.id == $("#testAutomationProject").val();
    })[0];

    if(objProject == undefined){
        console.log('objProject undefined')
        objProject = select_project;
    }

    console.log(select_project);
    loadTemplate(select_project);


    $(".pearls").children().addClass('done');


    $("#exampleWizardFormContainer").wizard('goTo', 3);

    // alert($("#testAutomationProject").val());
   /* 

    console.log(select_project);

    if(select_project.testSuites.length == 0){
        $('.existingTestSuite').find('h3').text('No Test Suites Found');
    }else{
        $('.existingTestSuite').find('h3').text('Select Test Suite');
    }

    $("#pg_ExistingTestSuite").load('template.html #registrantMembers', function () {
        var options = {
            templateId: '#registrantMembers',
            msgContainerId: '#pg_ExistingTestSuite'
        };

        var msg = {};
        msg.tsObject = select_project.testSuites;
        var template = $(options.templateId).html();
        var msgContainer = $(options.msgContainerId);
        Mustache.parse(template);
        var rendered = Mustache.render(template, msg);
        msgContainer.html(rendered);

    });*/

    /***show predefined ts */
  /*  $("#tsstep2 .existingTestSuite").show();
  $("#tsstep2 .newTestSuite").hide();*/
});

   $(document).on("click", "#btnCreateNewTestSuite", function () {
    $(".newTestSuite").slideDown();
});


   /**add steps */

   $(document).on("click", "#btnAddSteps", function () {
    var stepCount = $("#pg_Steps").find(".panel").length;
    var cloneparent = $("#pg_Steps");
    cloneparent.find(".panel:first-child .panel-action").show();
    var eleToClone = cloneparent.find(".panel:first-child");
    var clonedElem = eleToClone.clone();

    //console.log("stepcount:"+stepCount);

    var newCloneNum = stepCount + 1;


    clonedElem.find("#stepNo").html(newCloneNum);
    clonedElem.find(".btnAction").attr("data-target", "#actStep" + newCloneNum).attr("aria-controls", "#actStep" + newCloneNum);
    clonedElem.find(".panelActions").attr("id", "actStep" + newCloneNum);
    clonedElem.find(".stepDescription").val("");
    clonedElem.find(".stepDescription").attr("id", "stepDescription" + newCloneNum);
    clonedElem.find(".stepDescription").attr("id", "stepDescription" + newCloneNum);

    clonedElem.find(".checkbox-custom input").prop("checked", false);
    clonedElem.find(".checkbox-custom input").attr("id", "inputUnchecked" + newCloneNum);
    clonedElem.find(".checkbox-custom label").attr("for", "inputUnchecked" + newCloneNum);


    clonedElem.find(".actionType").attr("id", "actionType" + newCloneNum);
    clonedElem.find(".dataCol").attr("id", "dataColoumn" + newCloneNum);
    clonedElem.find(".locatorType").attr("id", "locatorType" + newCloneNum);
    clonedElem.find(".locatorNameVal").attr("id", "locatorNameVal" + newCloneNum);


    clonedElem.find(".dataColoumn").hide();
    clonedElem.find(".elelocatorType").hide();
    clonedElem.find(".elelocatorNameVal").hide();
    clonedElem.find("#dataColoumn" + newCloneNum).val("");
    clonedElem.find("#locatorNameVal" + newCloneNum).val("");
    cloneparent.append(clonedElem);

    stepCount++;


});


   /***steps dropdown logic */

   $(document).on("change", ".actionType", function () {
    var actionTypeId = $(this).attr('id');
    // alert(actionTypeId);
    var actionVal = $(this).val();
    var actParent = $(this).parents(".actionRow");
    var isUserAction = $('#' + actionTypeId).find('option:selected').attr('isUserAction');

    if (isUserAction == "true") {

        if (actionVal == "launch") {
            actParent.find("#labelDc").text("Enter URL");
            actParent.find(".dataColoumn").show();
        } else {
            actParent.find("#labelDc").text("Enter Data Coloumn");
            actParent.find(".elelocatorType").show();
            actParent.find(".dataColoumn").show();
        }


    } else if (isUserAction == "false") {

    }

    actParent.find(".elelocatorType").show();

    if ($(this).val() == "launch") {
        // actParent.find("label").text("Enter URL");
        actParent.find(".dataColoumn").show();
        actParent.find(".elelocatorType").hide();
    }

});

   $(document).on("change", ".locatorType", function () {
    var locatorTypeId = $(this).attr('id');
    // alert(locatorTypeId)
    var locatorVal = $(this).val();
    var isIdentifier = $('#' + locatorTypeId).find(':selected').attr('isIdentifier');

    var actParent = $(this).parents(".actionRow");

    //Condition for textbox -  working
    //if(isIdentifier == "true"){
        console.log("locatorVal: " + locatorVal);
        if (locatorVal == "id") {
            actParent.find("#labelLname").text("Enter ID");
            actParent.find(".elelocatorNameVal").show();
        } else if (locatorVal == "name") {
            actParent.find("#labelLname").text("Enter Name");
            actParent.find(".elelocatorNameVal").show();
        } else if (locatorVal == "linktext") {
            actParent.find("#labelLname").text("Enter Link Test");
            actParent.find(".elelocatorNameVal").show();
        } else if (locatorVal == "partiallink") {
            actParent.find("#labelLname").text("Enter Partial Link");
            actParent.find(".elelocatorNameVal").show();
        } else if (locatorVal == "xpath") {
            actParent.find("#labelLname").text("Enter XPath");
            actParent.find(".elelocatorNameVal").show();
        } else if (locatorVal == "class") {
            actParent.find("#labelLname").text("Enter Class");
            actParent.find(".elelocatorNameVal").show();
        } else if (locatorVal == "css") {
            actParent.find("#labelLname").text("Enter Css");
            actParent.find(".elelocatorNameVal").show();
        } else if (locatorVal == "span") {
            actParent.find("#labelLname").text("Enter Span");
            actParent.find(".elelocatorNameVal").show();
        } else {
            actParent.find("#labelLname").text("Enter Name");
            actParent.find(".elelocatorNameVal").show();
        }


    });

   function pageCounter(status) {
    if (status == 'CreateProject') {
        // alert("New Project Created");
    } else if (status == 'existingProject') {
        // alert("Existing Project");
    }
}



/**select2 scroll problem */
$('select').on('select2:open', function (evt) {
    $("html,body").css("overflow-y", "hidden");
});
$('select').on('select2:close', function (evt) {
    $("html,body").css("overflow-y", "auto");
});

/****wizard btn clicks */

function clickWizardNext() {
    $(".wizard-buttons > a.btn[data-wizard='next']").click();
}

function clickWizardBack() {
    $(".wizard-buttons > a.btn[data-wizard='back']").click();
}


/**section1 */
$(document).on("click", ".tsSection1 .btnBack", function () {
    clickWizardBack();
});

$(document).on("click", ".tsSection1 .btnNext", function () {

     //Project Name
     var projectName = $("#projectName").val();
     var applicationType = $('#applicationType').val();

     var selectProject = {};

     var tagsSet = '';


     var tagLabels = $('.tagLabel');

     var rolesLink = $('#projectRoles li');

     var tagsArray = [];
     var rolesArray = [];
     var userProjects= [];
     var createObject = {};

    // var userProjectRoles = [];
    //var projectRoles = {}; 

    // get all Tags
    tagLabels.each(function () {
        var tag = $(this).data('.token-input');
        tagsArray.push(tag);
        console.log(tagsArray);
    });
    
    
    rolesLink.each(function (i) {
        var roleName = $(this).text();
        var panelName = "panel"+roleName;
        console.log("panleName: " +panelName)
        if(roleName.length > 0 && roleName != ''){

         //Selected Roles
         var arrayOfValues = [];

         $("#"+panelName).find('#tableRoles tr td i').each(function() {
            arrayOfValues.push($(this).hasClass('rActive'));
        })


         var projectRoles = {};


         var userProjectRoles = [];
         var userId = [];

         $("#"+panelName).find("#memberListGroup >li").each(function (j) {

            userId[j]= $(this).find(".personName").attr('useremail');
            var rl = {};
            rl.userId = userId[j];

            userProjectRoles.push(rl);
            
        });

         var viewTestScript = arrayOfValues[0];  
         var createTestScript = arrayOfValues[1];
         var editTestScript = arrayOfValues[2];
         var deleteTestScript = arrayOfValues[3];

         var viewTestSuite = arrayOfValues[5];
         var createTestSuite = arrayOfValues[6];
         var editTestSuite = arrayOfValues[7];
         var deleteTestSuite = arrayOfValues[8];

         projectRoles["roleName"] = roleName;

         projectRoles["viewTestScript"] = viewTestScript;
         projectRoles["createTestScript"] = createTestScript;
         projectRoles["editTestScript"] = editTestScript;
         projectRoles["deleteTestScript"] = deleteTestScript;

         projectRoles["viewTestSuite"] = viewTestSuite;
         projectRoles["createTestSuite"] = createTestSuite;
         projectRoles["editTestSuite"] = editTestSuite;
         projectRoles["deleteTestSuite"] = deleteTestSuite;
         projectRoles["userProjectRoles"] = userProjectRoles;

         rolesArray.push(projectRoles);
     }
 });
    
    //createObject.projectName = projectName;
    createObject["projectName"] = projectName;
    createObject["applicationType"] = applicationType;
    createObject["projectRoles"] = rolesArray;
    createObject["startDate"] = "2017-09-04T11:50:06.000+0000";
    createObject["teamId"] = localStorage.selectedTeamId;
    createObject["tagsSet"] = "test";
    
    console.log(JSON.stringify(createObject));
    console.log(createObject);
    objProject = jqServices.createProject(createObject);

    /**passing tags to other section */        
    var tagsVal = $(".tsSection1 .tagsField").val();        
    if(tagsVal.trim() != "") {      
        $(".tsSection2 .tagsField").tokenfield('destroy');      
        $(".tsSection2 .tagsField").val(tagsVal);       
        $(".tsSection2 .tagsField").tokenfield();       

    }

    $('.tsSection2').find(".btnBack").removeClass('disabled');

    clickWizardNext();
});


/**section2 */
$(document).on("click", ".tsSection2 .btnBack", function () {
    $('#pg_ExistingTestSuite').html('');
    $("#tsstep2 .newTestSuite").show();
    $("#tsstep2 .existingTestSuite").hide();
    backS2click = true;
    clickWizardBack();
});

$(document).on("click", ".tsSection2 .btnNext", function () {


    var testSuiteName = $('#testSuiteName').val();
    var testModuleName = $('#testModuleName').val();


    testSuite.testSuiteName = testSuiteName;
    testSuite.moduleName = testModuleName;

    //testSuites.push(testSuite);

    //console.log(JSON.stringify(testSuites));


    //objProject.push(testSuite);

    /**passing tags to other section */
    var tagsVal = $(".tsSection2 .tagsField").val();
    if (tagsVal.trim() != "") {
        $(".tsSection3 .tagsField").tokenfield('destroy');
        $(".tsSection3 .tagsField").val(tagsVal);
        $(".tsSection3 .tagsField").tokenfield();

    }

    console.log(objProject);
    console.log("before pushing");
    console.log(select_project);
    /*if(objProject == undefined){
        objProject = select_project;
    }*/
    objProject.testSuites.push(testSuite);
    objProject = jqServices.updateProject(objProject);
    console.log('after updating');
    console.log(objProject);


    var testSuiteName = $("#testSuiteName").val();
    $("#testSuiteNameVal").html(testSuiteName);
    var testModuleName = $("#testModuleName").val();
    $("#moduleNameVal").html(testModuleName);



    clearStep3();
    $('.tsSection3').find(".btnBack").removeClass('disabled');

    clickWizardNext();

    // alert('check 3')

});

/**section3 */
$(document).on("click", ".tsSection3 .btnBack", function () {
    backS3click = true;
    clickWizardBack();
});

$(document).on("click", ".tsSection3 .btnNext", function () {



    var testScripts = [];
    var testScript = {};
    var testSteps = [];
    var testScriptName = $('#testScriptName').val().trim();

    if(testScriptName.trim() == ""){
        alert('Enter Test Script Name')
        return false;
    }

    testScript.testScriptName = testScriptName;

    var stepCount = $("#pg_Steps").find(".panel").length;
    for (var i = 1; i <= stepCount; i++) {
        var testStep = {};
        var userAction = $('#actionType' + i).find(":selected").text().trim();
        var dataColumn = $('#dataColoumn' + i).val().trim();
        var identifier = $('#locatorType' + i).find(":selected").text().trim();
        var identifierValue = $('#locatorNameVal' + i).val().trim();
        var stepDescription = $('#stepDescription' + i).val().trim();
        if(stepDescription.trim() == ""){
            alert('Enter Description')

            return false;
        }
        var controlType = "";
        var index = "";
        var property = "";
        var status = $('#actionType' + i + ' option:selected').attr('isuseraction');
        if (status == 'true') {
            var isDataColumn = true;
        } else {
            var isDataColumn = false;
        }
        if (userAction == "Launch") {
            var isDataColumn = true;
        }
        var dataValue = "";
        if ($('#inputUnchecked' + i).is(":checked")) {
            var isScreenshot = true;
        } else {
            var isScreenshot = false;
        }
        testStep.stepNum = i;
        testStep.stepDescription = stepDescription;
        testStep.controlType = 'WebElement';
        testStep.identifier = identifier;
        testStep.identifierValue = identifierValue;
        testStep.index = index;
        testStep.userAction = userAction;
        testStep.dataColumn = i;
        testStep.property = property;
        testStep.isScreenshot = isScreenshot;
        testStep.isDataColumn = isDataColumn;
        testStep.dataValue = dataColumn;
        testSteps.push(testStep);
    }
    testScript.testSteps = testSteps;

    // testScripts.push(testScript);

    //testScripts.testScripts = testScripts;
    ////console.log(testScripts);


    /*testSuite.testScripts = testScripts;
    testSuites.push(testSuite);
    objProject.testSuites = testSuites;*/

    var tsName = $('#testSuiteNameVal').text();
    // alert(tsName);
   /* if(objProject == undefined){
        console.log('objProject undefined')
        objProject = select_project;
    }*/

    var ts_i;
    var obj = objProject.testSuites;
    // console.log(obj);
    obj.some(function(entry, i) {
        if (entry.testSuiteName == tsName.trim()) {
            ts_i = i;
            return true;
        }
    });
    
    objProject.testSuites[ts_i].testScripts.push(testScript);
    //objProject.testSuite = testSuite;
    var tagsVal = $(".tsSection3 .tagsField").val();
    console.log("tagsVal:="+tagsVal);

    var tagsArray = [];
    tagsArray = tagsVal.split(',');
    

    objProject.tags = tagsArray;


    console.log(objProject)
    objProject = jqServices.updateProject(objProject);
    console.log(objProject);
    // alert('test suite template');
    loadTemplate(objProject);

    

    clickWizardNext();
});

/**section4 */
$(document).on("click", ".tsSection4 .btnBack", function () {
    clickWizardBack();
});

$(document).on("click", ".tsSection4 .btnNext", function () {

    alert('Finish');

});



/**navigate wizard via jquery */

$(document).on("click", "#btnNewTestSuite", function () {

    $('#testSuiteName').val("");
    $('#testModuleName').val("");

    $('.tsSection2').find(".btnBack").addClass('disabled');
    $("#exampleWizardFormContainer").wizard('goTo', 1);
});

$(document).on("click", "#btnAddTs", function () {

    clearStep3();

    var suitName = $(this).closest('.panel').find('.suitName').text();
    var modName = $(this).closest('.panel').find('.modName').val();
    

    $('#testSuiteNameVal').text(suitName);
    $('#moduleNameVal').text(modName);
    $('.tsSection3').find(".btnBack").addClass('disabled');


    $("#exampleWizardFormContainer").wizard('goTo', 2);

});

$(document).on("click", ".editTestScriptBtn", function () {

    $('.tsSection3').find(".btnBack").addClass('disabled');

    var tscNameTem = $(this).closest(".panel-heading").first().text();
    console.log(tscNameTem.trim());

    var tsObj = [];

    var arr = objProject.testSuites;
    console.log(arr);
    arr.forEach(function(e) {
        tsObj = tsObj.concat(e.testScripts.filter(function(c) {
            return (c.testScriptName === tscNameTem.trim());
        }));
    });

    console.log(tsObj);
    var testScript = tsObj[0];
    console.log(testScript);

    $(".tsSection3").find("#testScriptName").val(testScript.testScriptName);
    $(".tsSection3").find("#stepDescription1").val(testScript.testSteps[0].stepDescription);
    console.log(testScript.testSteps[0].userAction);
    $(".tsSection3").find("#actionType1").val("new");

    console.log(objProject);
    $("#exampleWizardFormContainer").wizard('goTo', 2);
});


/**execute run screen */

$(".btnExecuteRun").click(function () {
   /* var testSuite;
    for (var i = 0; i < objProject.testSuites.length; i++) {
        testSuite = objProject.testSuites[i];
        console.log(testSuite.id);
    }
    jqServices.createTestScript(testSuite.id);
    window.open('test_automation_result.html?id='+testSuite.id,'_blank');*/

    console.log(tsId);
    jqServices.createTestScript(tsId);
    window.open('test_automation_result.html?id='+tsId,'_blank');
    console.log("after window");
});



/*LOAD MUSTACHE TEMPLATE */

function loadTemplate(projectObj){

    $("#tsuitePanel").load('template5.html #registrantMembers', function () {
        var options = {
            templateId: '#registrantMembers',
            msgContainerId: '#tsuitePanel'
        };

        var msg = {};
        msg.tsObject = projectObj.testSuites;
        var template = $(options.templateId).html();
        var msgContainer = $(options.msgContainerId);
        Mustache.parse(template);
        var rendered = Mustache.render(template, msg);
        msgContainer.html(rendered);

    });
}

/* CLEAR STEP 3 FORM */

function clearStep3(){
   $('#testScriptName').val("");
   $('#stepDescription1').val("");
   $('#inputUnchecked1').attr('checked', false);
   $('#dataColoumn1').val("");
   $('#locatorNameVal1').val("");
   $('#actionType1').find("option:selected").removeAttr("selected");
   $('#locatorType1').find("option:selected").removeAttr("selected");
 // $('#pg_Steps').not('div:first').remove();
 $('.tsSection3 #pg_Steps .panel:not(:first)').remove();
 console.log(objProject);
}

function getObjects(obj, key, val, newVal) {
    var newValue = newVal;
    var objects = [];
    for (var i in obj) {
        if (!obj.hasOwnProperty(i)) continue;
        if (typeof obj[i] == 'object') {
            objects = objects.concat(getObjects(obj[i], key, val, newValue));
        } else if (i == key && obj[key] == val) {
            obj[key] = newValue;
        }
    }
    return obj;
}

}) // ready ends here

