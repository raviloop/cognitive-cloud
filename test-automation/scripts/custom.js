

$(window).load(function() {
    $("#aside").load("includes/left_menu.html");
    $("#app-header").load("includes/header.html");
})


$(function () {
    // Handler for .ready() called.  ------ Dipesh
    if ($("#btn-save-2").length) {
        // $(".submit").click(function () {
        //     $(".alert-success").css("display", "block");
        //     $("html, body").animate({ scrollTop: 0 }, 600);
        //     window.setTimeout(function () {
        //         location.href = "user_control_center.html";
        //     }, 3000);
        // });
        // btn-save
        $('#btn-save-2').on('click', function (e) {
            new PNotify({
                //title: 'Primary notice',
                text: 'Team created successfully!',
                icon: 'icon-checkmark4',
                addclass: 'bg-success'
            });
            window.setTimeout(function () {
                location.href = "user_control_center.html";
            }, 3000);
        });
    }

    // collapse Handler - Pravin - 16-Mar-17
    $('.panel-group').on('show.bs.collapse', function (e) {
        // resize footable in side collapse panel
        $(window).trigger('resize');
    });

    $('a[href="#"]').attr('href','javascript:void(0)');


    // Enable/disable Form controls
    /*$('.btn-edit').each(function () {

        var cur = $(this);
        cur.on('click', function (e) {

              e.preventDefault();
              var panel = cur.closest('.panel');
              var panelHeading = cur.closest('.panel-heading');
              var fieldset = panel.find('fieldset');

              //
              //panelHeading.find('a[data-toggle="collapse"]').trigger('click');
              if ($(fieldset).prop('disabled') === true) {
                  $(fieldset).prop('disabled', false);
              }
              else {
                  $(fieldset).prop('disabled', true);
              }
              //
              $(this).parents(".panel").find('th span').toggle();
              $('.footable-toggle', fieldset).removeClass('hidden');



        });
    });*/


    // Dipesh ------------------service store of user dashbord
    if($('body[data-page="user-dashbord"]').length){
        // console.log("chutya");
        $(".click").click(function(){
            if($("#serviceStoreDiv").hasClass("panel-collapsed"))
            {
                $("#serviceStoreDiv").removeClass('panel-collapsed');
                $(".store-content").css("display" , "block");
                $(".show-store").text($(".show-store").text() == 'Show Store' ? 'Hide Store' : 'Show Store');
            }
            else
            {
                $("#serviceStoreDiv").addClass('panel-collapsed');
                $(".store-content").css("display" , "none");
                $(".show-store").text($(".show-store").text() == 'Hide Store' ? 'Show Store' : 'Hide Store');
            }
        });

        $(".sidebar-main-toggle").click(function(){
            if($(".pace-done").hasClass("sidebar-xs"))
            {
                $(".service-store").css("width" , "calc(100% - 300px)");
            }
            else
            {
                $(".service-store").css("width" , "calc(100% - 95px)");
            }
        });

        $(".purchased-btn").click(function(){
            $(".before-purchase").css("display" , "none");
            $(".after-purchase").css("display" , "block");

            window.setTimeout(function () {
                location.href = "my-account.html";
            }, 0);

        });

        $(".purchased-btn-devOps").click(function(){
            $(".before-purchase-Devops").css("display" , "none");
            $(".after-purchase-Devops").css("display" , "block");

            window.setTimeout(function () {
                location.href = "my-account.html";
            }, 0);

        });

        $(".purchased-btn-ser-virtu").click(function(){
            $(".before-purchase-ser-virtu").css("display" , "none");
            $(".after-purchase-ser-virtu").css("display" , "block");

            window.setTimeout(function () {
                location.href = "my-account.html";
            }, 0);

        });

        // ---------------------/service store of user dashbord
    }



    // service-virtu page HANDLER
    if ($('body[data-page="service-virtu"]')) {
        //console.log('service-virtu');

        var accordion_dependency = $('#accordion-dependency');

        /// create new dependency
        $('#btn-add').on('click', function (e) {
            //e.preventDefault();

            var cnt = $('.panel', accordion_dependency).length;
            var href = "dependency-group-0" + cnt;
            var panel = $('.panel:first', '#accordion-dependency').clone();

            //update href
            var toggle = $('a[data-toggle="collapse"]', panel).attr('href',"#" + href).addClass('collapsed');
            $('.panel-collapse', panel).attr('id', href);
            $('.btn-record', panel).removeClass('stop').find('span').removeClass('animated flash infinite').html('Start Recording');


            panel.appendTo(accordion_dependency);
            toggle.trigger('click');
        });

        //on txt focus
        //var txtUrl = $('.txt-url', accordion_dependency);
        accordion_dependency.on('focus', '.txt-url', function (e) {
            var cur = $(this);
            var toggle = cur.parents('.panel-heading').find('.accordion-toggle');
            if (toggle.hasClass('collapsed')) {
                toggle.trigger('click');
            }
        });

        //Recording Button Handler
        accordion_dependency.on('click', '.btn-record', function (e) {
            var cur = $(this);
            var span = $('span', cur);

            if (cur.hasClass('stop')) {
                // recording stopped
                cur.removeClass('stop');
                span.html('Start Recording').removeClass('animated flash infinite');
                //
                cur.parents('.panel-body').find('.recorded-traffic').removeClass('hidden');

            }
            else {
                // recording started
                cur.addClass('stop');
                span.html('Stop Recording').addClass('animated flash infinite');
                var url = cur.parentsUntil('.panel-heading').find('.txt-url').val();
                //
                window.open(url, "", "width=400,height=400");

                $('#btn-back').removeClass('hidden');
                $('#btn-continue').addClass('hidden');
                $('#btn-save').addClass('bg-blue-400').prop('disabled',false);
            }
        });

        // btn-save
        $('#btn-save').on('click', function (e) {
            new PNotify({
                //title: 'Primary notice',
                text: 'Your configuration has been saved successfully!',
                icon: 'icon-checkmark4',
                addclass: 'bg-success'
            });
        });
    }

    // ==========================service-store-block
    var service_store_block = (function () {
        var block = $('#service-store-block'),
            wrapper = $('.service-wrap', this.block),
            toggler = $('.btn-toggle', this.block),
            panelBody = $('.panel-body', block);

        // toggle icon
        var toggle = function () {
            toggler.on('click', function (e) {
                //console.log('toggle icon');
                var icon = $('i.glyphicon', $(this));
                if (icon.hasClass('glyphicon-chevron-up')) {
                    icon.removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
                    panelBody.show();
                    block.addClass('hasBackDrop');
                }
                else {
                    icon.removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
                    panelBody.hide();
                    block.removeClass('hasBackDrop');
                }
                //// ============Calculate height added for service store=========== ///////
                var wH = $(window).height();
                var pH = $('.panel-heading', wrapper).outerHeight();
                if (window.matchMedia('(min-width:320px) and (max-width:767px)')) {
                    panelBody.css('max-height', (wH + pH));
                    //console.log('320px');
                } else {
                    panelBody.css('max-height', (wH + pH) - 10);
                }
                //// ============Calculate height added for iphone service store=========== ///////
            });
        };

        var toggleBlock = function () {
            $('#uctrlTabs a').on('click', function (e) {
                var cur = $(this);
                $("html, body").animate({ scrollTop: cur.offset().top-50 }, 600);
                if (cur.hasClass('serviceTab')) {
                    block.removeClass('hidden');}
                else {
                    //
                    block.insertBefore($('.content > .panel.panel-flat'));
                    block.addClass('hidden');
                }
            });
        };

        var init = function () {
            toggle();
            panelBody.hide();
            toggleBlock();

            $('body').on('click', '.hasBackDrop', function (e) {
                if ($(e.target).is('.hasBackDrop')) {
                    toggler.trigger('click');
                }
            });

            $('body').on('click', '.click', function (e) {
                    toggler.trigger('click');
            });

            var section_001 = $('#section-001'),
                section_002 = $('#section-002'),
                section_003 = $('#section-003')
                tabbable = $('.tabbable');

            $('.screen1').on('click', function (e) {
                var hdr = $(".hdr");
                tabbable.addClass('hidden'); //
                section_001.addClass('hidden');
                section_002.removeClass('hidden');
                section_003.addClass('hidden');
                $(".hasBackDrop .panel-body").animate({ scrollTop: 0 }, 600);
            });

            $('.screen').on('click', function (e) {
                tabbable.removeClass('hidden'); //
                section_001.removeClass('hidden');
                section_002.addClass('hidden');
                section_003.addClass('hidden');
                $(".hasBackDrop .panel-body").animate({ scrollTop: 0 }, 600);
            });

            $('.screen2').on('click', function (e) {
                section_001.addClass('hidden');
                section_002.addClass('hidden');
                section_003.removeClass('hidden');
                $(".hasBackDrop .panel-body").animate({ scrollTop: 0 }, 600);
            });

        };

        return {init: init}
    })();
    service_store_block.init();

    $(document).ready(function () {
        $(".ckbCheckAll").click(function () {
            $(".checkBoxClass").prop('checked', $(this).prop('checked'));
        });

        $(".checkBoxClass").change(function(){
            if (!$(this).prop("checked")){
                $(".ckbCheckAll").prop("checked",false);
            }
        });

        $(".bg-danger").click(function () {
            $(".checkBoxClass").prop('checked', true);
            $(".ckbCheckAll").prop('checked' , true);
        });
    });



    /*============== pipeline-summary - Pravin 24-03-17============*/
    var pipelinesummary = (function () {
        var toggler = $('section:first-of-type i, header', '.pipeline-summary li');

        var render = function () {
            if (window.matchMedia("(min-width: 320px) and (max-width: 767px)").matches) {
                $('.pipeline-summary li > section:not(:first-of-type)').hide();

                $('.pipeline-summary li').each(function () {
                    var cur = $(this);
                    var sec = $('section:first-of-type section', cur);
                    var i = $('i.icon', cur);
                    sec.append(i);
                    console.log(i);
                });
            }
            else {
                $('.pipeline-summary li > section main').hide();
            }
        };
        var evtHandler = function () {
            toggler.on('click', function (e) {
                if (window.matchMedia("(min-width: 320px) and (max-width: 767px)").matches) {
                    var li = $(this).parents('li');
                    li.toggleClass('active').siblings().removeClass('active');
                    $('section:not(:first-of-type)', li).slideToggle();
                    $('section:not(:first-of-type)', li.siblings()).slideUp();
                }
                else {
                    $(this).parents('li').toggleClass('active').find('main').slideToggle();
                    $(this).parents('li').siblings().removeClass('active').find('main').slideUp();
                }
            });
        };
        var init = function () {
            render();
            evtHandler();
        };
        return { init: init }
    })();
    pipelinesummary.init();
    /*============== /pipeline-summary - Pravin 24-03-17============*/
});


//change aside Menu

function changeAsideBar() {
    $("#aside").toggleClass("folded");
    $("#service-store-block").toggleClass("navunfolded");
}


//delete panel



//navicons on mobile
$(".btnIconNav").click(function() {
  $(".navIcons").slideToggle();
});


$(".myPlans").click(function() {
  $(".myPlansDiv").slideToggle(function() {

  });
})

$(".btnSaveEditChange").click(function() {
  showAlert("Changes updated successfully");
})
//show alert
function showAlert(text) {
  $(".alert").fadeIn(function() {
    $(this).find("strong").html("");
    $(this).find("strong").html(text);
  });
  //$("html, body").animate({ scrollTop: 0 }, 600);

  window.setTimeout(function () {
      $(".alert").fadeOut();
  }, 3000);

  if(text === "team added successfully") {
    window.setTimeout(function () {
      location.href = "user_control_center.html";
  }, 3000);
  }
}


//focus on panel heading on-panel-open
/*$('.panel').on('shown.bs.collapse', function (e) {
       var offset = $(this).offset();
       if(offset) {
           $('html,body').animate({
               scrollTop: $(this).find("a").offset().top-60
           }, 500);
       }
   });*/


$(".btnAddMoreMember").click(function() {
  var $parent =   $(this).parents(".panel-body");
  var $appendToElement = $parent.find("tbody");
  $parent.find("tbody tr:last").clone().appendTo($appendToElement);
})

$('.modal').on('show.bs.modal', function (e) {
  $("html,body").css("overflow-y", "hidden");
})

$('.modal').on('hide.bs.modal', function (e) {
    $("html,body").css("overflow-y", "auto");
})


































/****seervice store***/

$(window).load(function() {
 	$(".tab-2").css("display", "none");
 	$(".tab-3").css("display", "none");

 	$(".screen").click(function () {
	    $(".tab-1").css("display", "block");
	    $(".tab-3").css("display", "none");
	    $(".tab-2").css("display", "none");
	    $(".store-content").animate({ scrollTop: 0 }, 600);
	});

 	$(".screen1").click(function () {
	    $(".tab-1").css("display", "none");
	    $(".tab-3").css("display", "none");
	    $(".tab-2").css("display", "block");
	    $(".store-content").animate({ scrollTop: 0 }, 600);
	});

	$(".screen2").click(function () {
	    $(".tab-1").css("display", "none");
	    $(".tab-2").css("display", "none");
	    $(".tab-3").css("display", "block");
	    $(".store-content").animate({ scrollTop: 0 }, 600);
	});
});



$(function () {
    // Handler for .ready() called.  ------ Dipesh
    if ($("#btn-save-2").length) {
        // $(".submit").click(function () {
        //     $(".alert-success").css("display", "block");
        //     $("html, body").animate({ scrollTop: 0 }, 600);
        //     window.setTimeout(function () {
        //         location.href = "user_control_center.html";
        //     }, 3000);
        // });
        // btn-save
        $('#btn-save-2').on('click', function (e) {
            new PNotify({
                //title: 'Primary notice',
                text: 'Team created successfully!',
                icon: 'icon-checkmark4',
                addclass: 'bg-success'
            });
            window.setTimeout(function () {
                location.href = "user_control_center.html";
            }, 3000);
        });
    }

    // collapse Handler - Pravin - 16-Mar-17
    $('.panel-group').on('show.bs.collapse', function (e) {
        // resize footable in side collapse panel
        $(window).trigger('resize');
    });

    $('a[href="#"]').attr('href','javascript:void(0)');


    // Enable/disable Form controls
    /*$('.btn-edit').each(function () {
        var cur = $(this);
        cur.on('click', function (e) {
            e.preventDefault();
            var panel = cur.closest('.panel');
            var panelHeading = cur.closest('.panel-heading');
            var fieldset = panel.find('fieldset');

            //
            //panelHeading.find('a[data-toggle="collapse"]').trigger('click');
            if ($(fieldset).prop('disabled') === true) {
                $(fieldset).prop('disabled', false);
            }
            else {
                $(fieldset).prop('disabled', true);
            }
            //
            $('span', fieldset).toggleClass('hidden');
            $('.footable-toggle', fieldset).removeClass('hidden');

        });
    });*/


    // Dipesh ------------------service store of user dashbord
    if($('body[data-page="user-dashbord"]').length){
        // console.log("chutya");
        $(".click").click(function(){
            if($("#serviceStoreDiv").hasClass("panel-collapsed"))
            {
                $("#serviceStoreDiv").removeClass('panel-collapsed');
                $(".store-content").css("display" , "block");
                $(".show-store").text($(".show-store").text() == 'Show Store' ? 'Hide Store' : 'Show Store');
            }
            else
            {
                $("#serviceStoreDiv").addClass('panel-collapsed');
                $(".store-content").css("display" , "none");
                $(".show-store").text($(".show-store").text() == 'Hide Store' ? 'Show Store' : 'Hide Store');
            }
        });

        $(".sidebar-main-toggle").click(function(){
            if($(".pace-done").hasClass("sidebar-xs"))
            {
                $(".service-store").css("width" , "calc(100% - 300px)");
            }
            else
            {
                $(".service-store").css("width" , "calc(100% - 95px)");
            }
        });

        $(".purchased-btn").click(function(){
            $(".before-purchase").css("display" , "none");
            $(".after-purchase").css("display" , "block");

            window.setTimeout(function () {
                location.href = "my-account.html";
            }, 0);

        });

        $(".purchased-btn-devOps").click(function(){
            $(".before-purchase-Devops").css("display" , "none");
            $(".after-purchase-Devops").css("display" , "block");

            window.setTimeout(function () {
                location.href = "my-account.html";
            }, 0);

        });

        $(".purchased-btn-ser-virtu").click(function(){
            $(".before-purchase-ser-virtu").css("display" , "none");
            $(".after-purchase-ser-virtu").css("display" , "block");

            window.setTimeout(function () {
                location.href = "my-account.html";
            }, 0);

        });

        // ---------------------/service store of user dashbord
    }



    // service-virtu page HANDLER
    if ($('body[data-page="service-virtu"]')) {
        //console.log('service-virtu');

        var accordion_dependency = $('#accordion-dependency');

        /// create new dependency
        $('#btn-add').on('click', function (e) {
            //e.preventDefault();

            var cnt = $('.panel', accordion_dependency).length;
            var href = "dependency-group-0" + cnt;
            var panel = $('.panel:first', '#accordion-dependency').clone();

            //update href
            var toggle = $('a[data-toggle="collapse"]', panel).attr('href',"#" + href).addClass('collapsed');
            $('.panel-collapse', panel).attr('id', href);
            $('.btn-record', panel).removeClass('stop').find('span').removeClass('animated flash infinite').html('Start Recording');


            panel.appendTo(accordion_dependency);
            toggle.trigger('click');
        });

        //on txt focus
        //var txtUrl = $('.txt-url', accordion_dependency);
        accordion_dependency.on('focus', '.txt-url', function (e) {
            var cur = $(this);
            var toggle = cur.parents('.panel-heading').find('.accordion-toggle');
            if (toggle.hasClass('collapsed')) {
                toggle.trigger('click');
            }
        });

        //Recording Button Handler
        accordion_dependency.on('click', '.btn-record', function (e) {
            var cur = $(this);
            var span = $('span', cur);

            if (cur.hasClass('stop')) {
                // recording stopped
                cur.removeClass('stop');
                span.html('Start Recording').removeClass('animated flash infinite');
                //
                cur.parents('.panel-body').find('.recorded-traffic').removeClass('hidden');

            }
            else {
                // recording started
                cur.addClass('stop');
                span.html('Stop Recording').addClass('animated flash infinite');
                var url = cur.parentsUntil('.panel-heading').find('.txt-url').val();
                //
                window.open(url, "", "width=400,height=400");

                $('#btn-back').removeClass('hidden');
                $('#btn-continue').addClass('hidden');
                $('#btn-save').addClass('bg-blue-400').prop('disabled',false);
            }
        });

        // btn-save
        $('#btn-save').on('click', function (e) {
            new PNotify({
                //title: 'Primary notice',
                text: 'Your configuration has been saved successfully!',
                icon: 'icon-checkmark4',
                addclass: 'bg-success'
            });
        });
    }

    // ==========================service-store-block
    var service_store_block = (function () {
        var block = $('#service-store-block'),
            wrapper = $('.service-wrap', this.block),
            toggler = $('.btn-toggle', this.block),
            panelBody = $('.panel-body', block);

        // toggle icon
        var toggle = function () {
            toggler.on('click', function (e) {
                //console.log('toggle icon');
                var icon = $('i.glyphicon', $(this));
                if (icon.hasClass('glyphicon-chevron-up')) {
                    icon.removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
                    panelBody.show();
                    block.addClass('hasBackDrop');
                }
                else {
                    icon.removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
                    panelBody.hide();
                    block.removeClass('hasBackDrop');
                }
                //// ============Calculate height added for service store=========== ///////
                var wH = $(window).height();
                var pH = $('.panel-heading', wrapper).outerHeight();
                if (window.matchMedia('(min-width:320px) and (max-width:767px)')) {
                    panelBody.css('max-height', (wH + pH));
                    //console.log('320px');
                } else {
                    panelBody.css('max-height', (wH + pH) - 10);
                }
                //// ============Calculate height added for iphone service store=========== ///////
            });
        };

        var toggleBlock = function () {
            $('#uctrlTabs a').on('click', function (e) {
                var cur = $(this);
                $("html, body").animate({ scrollTop: cur.offset().top-50 }, 600);
                if (cur.hasClass('serviceTab')) {
                    block.removeClass('hidden');}
                else {
                    //
                    block.insertBefore($('.content > .panel.panel-flat'));
                    block.addClass('hidden');
                }
            });
        };

        var init = function () {
            toggle();
            panelBody.hide();
            toggleBlock();

            $('body').on('click', '.hasBackDrop', function (e) {
                if ($(e.target).is('.hasBackDrop')) {
                    toggler.trigger('click');
                }
            });

            $('body').on('click', '.click', function (e) {
                    toggler.trigger('click');
            });

            var section_001 = $('#section-001'),
                section_002 = $('#section-002'),
                section_003 = $('#section-003')
                tabbable = $('.tabbable');

            $('.screen1').on('click', function (e) {
                var hdr = $(".hdr");
                tabbable.addClass('hidden'); //
                section_001.addClass('hidden');
                section_002.removeClass('hidden');
                section_003.addClass('hidden');
                $(".hasBackDrop .panel-body").animate({ scrollTop: 0 }, 600);
            });

            $('.screen').on('click', function (e) {
                tabbable.removeClass('hidden'); //
                section_001.removeClass('hidden');
                section_002.addClass('hidden');
                section_003.addClass('hidden');
                $(".hasBackDrop .panel-body").animate({ scrollTop: 0 }, 600);
            });

            $('.screen2').on('click', function (e) {
                section_001.addClass('hidden');
                section_002.addClass('hidden');
                section_003.removeClass('hidden');
                $(".hasBackDrop .panel-body").animate({ scrollTop: 0 }, 600);
            });

        };

        return {init: init}
    })();
    service_store_block.init();

    $(document).ready(function () {
        $(".ckbCheckAll").click(function () {
            $(".checkBoxClass").prop('checked', $(this).prop('checked'));
        });

        $(".checkBoxClass").change(function(){
            if (!$(this).prop("checked")){
                $(".ckbCheckAll").prop("checked",false);
            }
        });

        $(".bg-danger").click(function () {
            $(".checkBoxClass").prop('checked', true);
            $(".ckbCheckAll").prop('checked' , true);
        });
    });



});


$(document).on("keydown",".note-editable",function() {
    $(this).parents(".note-editor").find(".note-placeholder").hide();
})


$('.btn-edit').click(function() {
    $(this).closest(".panel").find(".panel-collapse").collapse();
    $(this).closest(".panel").find('th span').toggle();

    var fieldset = $(this).closest(".panel").find("fieldset");

    if (fieldset.prop('disabled') === true) {
        fieldset.prop('disabled', false);
    }
    else {
        fieldset.prop('disabled', true);
    }
});

$("#user-nav-btn").click(function() {
    $(this).find("i").toggleClass("ion-chevron-up");
    $('#user-nav').collapse("toggle");
});
