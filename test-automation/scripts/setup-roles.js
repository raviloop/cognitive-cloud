

/*var s_num = 1;
$(".btnNext").click(function() {
  if(s_num <=4) {
    s_num++;

    var percentComplete = s_num * 20;
    $(".roleSteps .progress-bar").css("width",percentComplete+"%");

    $(".navStepsUl > li > a#a_tab"+s_num).addClass("completed");

  }
  else {
    s_num = 5;
  }
$("#tab3").removeClass("workFlowTab3");
  
});

$(".btnPrevious").click(function() {
  if(s_num >=2) {
      s_num--;

    var percentComplete = s_num * 20;
    $(".roleSteps .progress-bar").css("width",percentComplete+"%");
    $(".navStepsUl > li > a#a_tab"+(s_num+1)).removeClass("completed");

  }
  else {
    s_num = 1;
  }

  if($("#tab3").hasClass("active")) {
    $("#tab3").removeClass("workFlowTab3");
  }
  else
  {
    $("#tab3").addClass("workFlowTab3");
  }
});
*/




//screen2
$(".createrolewrap .md-input").keyup(function() {
  var roleName = $(".roleName").val();
  var roleAliasFieldVal = $(".roleAliasField").val();

  if((roleName.length > 0) && (roleAliasFieldVal.length > 0 )) {
    $(".roleAssignmentDiv").slideDown();
  }
});

//checkbox

$(".rolePowerCheckBox input").change(function() {
  if ($(this).is(':checked')) {
    $(this).parent().find("i").removeClass("ion-checkmark");
    $(this).parent().find("i").addClass("ion-close");
    $(this).parent().find("i").css("color","#ef193c");
  }
  else {
    $(this).parent().find("i").removeClass("ion-close");
    $(this).parent().find("i").addClass("ion-checkmark");
    $(this).parent().find("i").css("color","#22b66e");
  }
});

var rolePowerCheckBoxFlag = 0;
$(".btnRoleAction").click(function(){
  if(rolePowerCheckBoxFlag === 0) {
    $(this).parents("tr").find(".rolePowerCheckBox input").attr("disabled",false);
    $(this).parents("tr").find(".rolePowerCheckBox i").css("color","#fe0000");
    rolePowerCheckBoxFlag = 1; 
  }
  else
  {
    $(this).parents("tr").find(".rolePowerCheckBox input").attr("disabled",false);
    $(this).parents("tr").find(".rolePowerCheckBox i").css("color","#fe0000");
    rolePowerCheckBoxFlag = 0;
  }
  
});

$(document).on("ready",function(){
  $(".rolePowerCheckBox input").attr("disabled","true");
  fillSlaDropdown();
});


//***screen3********//


/***add member******/
$(document).on("click",".userSelectBtn",function() {
  $(".card").css("z-index",21);
  $(this).parents(".card").css("z-index",25);
  $(this).parents(".addMemberGrp").toggleClass("active");
  $(this).parent(".addMemberGrp").find("input[type=checkbox]").attr('checked', false).removeClass("memActive");
});



$(document).on("click",".addMember",function() {
  $(this).parents(".addMemberGrp").toggleClass("active");


  var memLength = $(this).parents(".addUserContent").find(".memberAddedList > ul > li").length;

  $('.addMemberGrp input[type="checkbox"]').each(function() {

      if ($(this).hasClass('memActive')) {
       var activeMemName =  $(this).parent().find("span").html();
       var memberLi = '<li class="col-lg-2 col-sm-4 col-xs-6"><div class="card"><span><i class="ion-ios-person" style="font-size: 3.2rem"></i></span><div class="card-block"><h6 class="card-title">'+activeMemName+'</h6><p style="display: block; height: 37px; padding-top: 6.4px; margin-top: 0px; padding-bottom: 3.2px; margin-bottom: 0px; overflow: hidden;"><a href="javascript:;" class="btn-del pull-left"><i class="ion-edit"></i></a><a href="javascript:;" class="btn-del pull-right" data-toggle="modal" data-target="#modal-deletemember"><i class="ion-ios-trash-outline"></i></a></p></div></div></li>';
      $(".addMember").parents(".addUserContent").find(".memberAddedList > ul").append(memberLi);
      memLength=memLength+ 1;
      $(".addMember").parents(".addUserHeader").find("p span").html(memLength);
      
      }
  })

   
});



$('.addMemberGrp input[type="checkbox"]').change(function() {
  if ($(this).is(':checked')) {
    $(this).addClass("memActive");
  }
})

$(".date .form-control").click(function() {
  $(this).parent().find(".input-group-addon").click();
})



/*$("#saveaddRole").click(function() {
  $(this).parents(".createrolewrap").find(".roleName").val("");
  $(this).parents(".createrolewrap").find(".roleAliasField").val("");
  $(this).parents(".roleAssignmentDiv").slideUp();
});*/

/*$("#roleEditCBtn").click(function() {
  $(".createrolewrap").parents(".createrolewrap").find(".roleName").val("");
  $(".createrolewrap").parents(".createrolewrap").find(".roleAliasField").val("");
  $(".roleAssignmentDiv").slideUp();
  $(".collapse").each(function() {
      $(this).slideUp();
  })
});*/

/**fill sla dropdon***/
function fillSlaDropdown() {
  $(".slaDay").each(function() {
    var slaDayOptLength = $(this).find("option").length
    console.log("slaDayOptLength"+ slaDayOptLength);
    if(slaDayOptLength < 2) {
      for(var day = 0; day<=31;day++)
      {
        $(this).append("<option value='"+day+"'>"+day+"</option>");
      }
    }
  });

  $(".slaHr").each(function() {
    var slaHrOptLength = $(this).find("option").length
    if(slaHrOptLength < 2) {
      for(var hr = 0; hr<=23;hr++)
      {
        $(this).append("<option value='"+hr+"'>"+hr+"</option>");
      }
    }
  });

  $(".slaMin").each(function() {
    var slaMinOptLength = $(this).find("option").length
    if(slaMinOptLength < 2) {
      for(var min = 0; min<=59;min++)
      {
        $(this).append("<option value='"+min+"'>"+min+"</option>");
      }
    }
  });
}


/*add new role btn***/

$(".addNewRoleBtn > a").click(function() {
  $(".liAddRole").slideDown();
})
$(".saveNewRoleBtn").click(function() {
  var roleNewName = $(this).parent().find(".roleName").val();
  if(roleNewName != "") {
  $(".liAddRole").hide();
  var roleCount = $(".ulRoleNames ul.nav").children().length -1;
  var nextNumber = roleCount + 1;

  var roleLeftMenu ='<li class="nav-item"><a href="#left-tab'+nextNumber+'" class="nav-link" data-toggle="tab" role="tab"><span>'+roleNewName+'</span><i class="ion-ios-trash-outline pull-right delNewUser"></i></a></li>';
  $(".liAddRole").before(roleLeftMenu);
  $(this).parent().find(".roleName").val("");

  var roleTabContent = $(".role_tab_pane_dynamic").clone();
  roleTabContent.removeClass("role_tab_pane_dynamic");
  roleTabContent.prop('id', 'left-tab'+nextNumber);
  roleTabContent.find(".roleAssignmentDiv p > strong").html(roleNewName);

  $(".divRoleDesc").append(roleTabContent);

  }
});


$(document).on("click",".delNewUser", function(e) {
  e.stopPropagation();
  $(this).parents("li").slideUp();
})








$(document).on({
    mouseenter: function () {
        $(this).find(".card-block p").stop().slideDown(300);
    },
    mouseleave: function () {
        $(this).find(".card-block p").stop().slideUp(300);
    }
}, ".memberAddedList > ul > li .card");



/***template-form*****/
var fieldStatus = 0;
$(".fieldIcon").click(function() {
  $(this).find("i").toggleClass("ion-eye-disabled");
  if(fieldStatus === 0) {
    $(this).parent().find(".form-control").attr("disabled",true);
    fieldStatus = 1;
  }
  else
  {
    $(this).parent().find(".form-control").attr("disabled",false);
    fieldStatus = 0;
  }
  
});

var dynamicFieldStatus = 0
$(document).on("click",".btnFieldStatus",function() {
  $(this).toggleClass("ion-eye-disabled"); 
  
  if(dynamicFieldStatus === 0) {
    $(this).parents(".form-field").find(".form-control").attr("disabled",true);
    dynamicFieldStatus = 1;
  }
  else
  {
    $(this).parents(".form-field").find(".form-control").attr("disabled",false);
    dynamicFieldStatus = 0;
  }
});

/**activate formbuilder*****/
$(".btnAddCustomField").click(function(){
  $(this).parents(".tabBody").find(".customField-editor").slideDown();
  $('html, body').animate({
        scrollTop: $(this).parents(".tabBody").find(".customField-editor").offset().top
  },1000);
  $(this).hide();
});



$(".btnAddTsSteps").click(function() {
  var stepTableTr = $(".testCaseSteps > .table > tbody > tr:last").clone();
  $(".testCaseSteps > .table > tbody").append(stepTableTr);
})




$(document).on("click",".btnWFinish",function() {
  $('#rootwizard .navStepsUl li:last-child').addClass('completedStep');
  showRsAlert(6);

});

function showRsAlert(num) {
  var alertDiv = document.createElement('div');
  var alertDivContent = "";
  var pglocation = "";
  $(alertDiv).addClass("alert").addClass("alert-success");
  $(alertDiv).attr("role", "alert");

  switch(num) {
   
    case 6:
    alertDivContent = "Role configured successfully";
    pglocation = "step_workspace.html";
    break;
   default:
    $(alertDiv).hide();
  }

  $(alertDiv).append("<p>"+alertDivContent+"</p>");
  
  $(".app-body").prepend($(alertDiv));
  $(alertDiv).fadeIn();

  


  window.setTimeout(function () {
    $(alertDiv).fadeOut().remove();
    location.href = pglocation;
  }, 6000);
}


