<nav class="site-navbar navbar navbar-inverse navbar-fixed-top navbar-mega" role="navigation">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle hamburger hamburger-close navbar-toggle-left hided"
    data-toggle="menubar">
    <span class="sr-only">Toggle navigation</span>
    <span class="hamburger-bar"></span>
  </button>
  <button type="button" class="navbar-toggle collapsed" data-target="#site-navbar-collapse"
  data-toggle="collapse">
  <i class="icon md-more" aria-hidden="true"></i>
</button>
<div class="navbar-brand navbar-brand-center site-gridmenu-toggle" data-toggle="gridmenu">
  <img class="navbar-brand-logo" src="assets/base/assets/images/logo.png" title="Cognitive Cloud">
  <span class="navbar-brand-text hidden-xs"> Cognitive Cloud</span>
</div>
<button type="button" class="navbar-toggle collapsed" data-target="#site-navbar-search"
data-toggle="collapse">
<span class="sr-only">Toggle Search</span>
<i class="icon md-search" aria-hidden="true"></i>
</button>
</div>
<div class="navbar-container container-fluid">
  <!-- Navbar Collapse -->
  <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
    <!-- Navbar Toolbar -->
    <ul class="nav navbar-toolbar">
      <li class="hidden-float" id="toggleMenubar">
        <a data-toggle="menubar" href="#" role="button">
          <i class="icon hamburger hamburger-arrow-left">
            <span class="sr-only">Toggle menubar</span>
            <span class="hamburger-bar"></span>
          </i>
        </a>
      </li>
      <li class="hidden-xs" id="toggleFullscreen">
        <a class="a_logo" href="http://www.quinnox.com/" target="_blank" role="button">
          <img src="assets/base/assets/images/c_logo.png">
        </a>
      </li>
      <li class="hidden-xs" id="toggleFullscreen">
        <a class="icon icon-fullscreen" data-toggle="fullscreen" href="#" role="button">
          <span class="sr-only">Toggle fullscreen</span>
        </a>
      </li>
      <li class="hidden-float">
        <a class="icon md-search" data-toggle="collapse" href="#" data-target="#site-navbar-search"
        role="button">
        <span class="sr-only">Toggle Search</span>
      </a>
    </li>

  </ul>
  <!-- End Navbar Toolbar -->
  <!-- Navbar Toolbar Right -->
  <ul class="nav navbar-toolbar navbar-right navbar-toolbar-right">

    <li class="dropdown">
      <a class="navbar-avatar dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false"
      data-animation="scale-up" role="button">
      <span class="avatar avatar-online">
        <!-- <img ng-src="{{vm.user.imageUrl}}" class="w-full rounded" alt="..."> -->
        <!--<img src="../../../global/portraits/5.jpg" alt="...">-->
        <div style="font-size: 10px !important;margin-top: 10px;">{{vm.user.firstName| limitTo : 1 }} {{vm.user.lastName| limitTo : 1 }}
        </div>
        <i></i>
      </span>
    </a>
    <ul class="dropdown-menu" role="menu">
      <li role="presentation">
        <a href="javascript:void(0)" role="menuitem"><i class="icon md-account" aria-hidden="true"></i> Profile</a>
      </li>
      <li role="presentation">
        <a href="javascript:void(0)" role="menuitem"><i class="icon md-card" aria-hidden="true"></i> Billing</a>
      </li>
      <li role="presentation">
        <a href="plans.html" role="menuitem"><i class="icon md-settings" aria-hidden="true"></i> Upgrade Plan</a>
      </li>
      <li class="divider" role="presentation"></li>
      <li role="presentation">
        <a href="javascript:void(0)" role="menuitem"><i class="icon md-power" aria-hidden="true"></i> Logout</a>
      </li>
    </ul>
  </li>

  <li id="toggleChat">
    <a data-toggle="site-sidebar" href="javascript:void(0)" title="Chat" data-url="../includes/right-bar.tpl">
      <i class="icon md-comment" aria-hidden="true"></i>
    </a>
  </li>
</ul>
<!-- End Navbar Toolbar Right -->
</div>
<!-- End Navbar Collapse -->
<!-- Site Navbar Seach -->
<div class="collapse navbar-search-overlap" id="site-navbar-search">
  <form role="search">
    <div class="form-group">
      <div class="input-search">
        <i class="input-search-icon md-search" aria-hidden="true"></i>
        <input type="text" class="form-control" name="site-search" placeholder="Search...">
        <button type="button" class="input-search-close icon md-close" data-target="#site-navbar-search"
        data-toggle="collapse" aria-label="Close"></button>
      </div>
    </div>
  </form>
</div>
<!-- End Site Navbar Seach -->
</div>
</nav>

<script type="text/javascript">
  // alert("herer");
</script>