(function() {
    'use strict';

    angular.module('app')
        .controller('TeamSelectorController', TeamSelectorController);

  TeamSelectorController.$inject = ['TeamService','AuthenticationService', 'UserService', '$rootScope', '$scope', '$window', '$filter','$location'];


    function TeamSelectorController(TeamService,AuthenticationService, UserService, $rootScope, $scope, $window, $filter, $location) {
        var vm = this;
        vm.user = null;
        vm.allUsers = [];

        initController();

        function initController() {
            loadCurrentUserandTeam();

        }

         function getAllTeam() {

            console.log("get team data was called");
            vm.allTeamData = [];

            TeamService.GetAllWithRole(vm.user.authorities[0]).then(function successCallback(response) {
                console.log("Success");
                console.log(response);
                vm.allTeamData = response.data;
              vm.allTeamDataBkp = angular.copy(vm.allTeamData);
              $rootScope.sessionGlobals.selectedTeam = undefined;
              AuthenticationService.SetSessionVariables();

            }, function errorCallback(response) {
                console.log("Error will retriving data");
            });
        }

      function loadCurrentUserandTeam() {
            UserService.GetByUsername($rootScope.globals.currentUser.username)
                .then(function(user) {
                    console.log(user);
                    vm.user = user.data;
                    if(vm.user.authorities[0]==="ROLE_ADMIN"){
                      vm.isAdmin = true;
                      vm.isUser=false;
                    }
                    else{
                      {
                        vm.isAdmin = false;
                        vm.isUser=true;
                      }
                    }
                    getAllTeam();
                });
        }
        $scope.selectTeam = function(teamId){
          $rootScope.sessionGlobals.selectedTeam =  $filter('filter')(vm.allTeamData, function (d) {return d.id === teamId;})[0]
          AuthenticationService.SetSessionVariables();
          $location.path("/userdashboard");
        }
        $scope.signout = function() {
            $window.localStorage.clear();
            $rootScope.globals = {};
        };
    }



})();
