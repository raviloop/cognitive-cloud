(function () {
    'use strict';

    angular.module('app')
        .controller('UserDashboardController', UserDashboardController);

    UserDashboardController.$inject = ['TeamService','$location', 'UserService','ServiceStoresService','$rootScope', '$scope','$window','$filter'];
    function UserDashboardController(TeamService,$location, UserService,ServiceStoresService,$rootScope,$scope,$window,$filter) {
        var vm = this;


        (function initController() {
           
           loadCurrentUser();
        })();

        function loadCurrentUser() {
            UserService.GetByUsername($rootScope.globals.currentUser.username)
                .then(function (user) {
                  console.log(user.data);
                    vm.user = user.data;
                    if(vm.user.authorities[0]==="ROLE_ADMIN"){
                      vm.isAdmin = true;
                      vm.isUser=false;
                    }
                    else{
                      {
                        vm.isAdmin = false;
                        vm.isUser=true;
                      }
                    }
                    getAllTeam()
                    $scope.getAllServices();
                });
        };
        function getAllTeam() {
          
                      console.log("get team data was called");
                      vm.allTeamData = [];
          
                      TeamService.GetAllWithRole(vm.user.authorities[0]).then(function successCallback(response) {
                          console.log("Success");
                          console.log(response);
                          vm.allTeamData = response.data;
                          if(vm.allTeamData.length==1){
                            $scope.selectedTeam=vm.allTeamData[0];
                            if($scope.selectedTeam){
                              $scope.selectedUserTeam = $scope.selectedTeam.teamUsers[0];
                              console.log($scope.selectedUserTeam )
                            }
                          }
                          console.log(vm.allTeamData)
                        vm.allTeamDataBkp = angular.copy(vm.allTeamData);
                        $rootScope.sessionGlobals.selectedTeam = undefined;
                        AuthenticationService.SetSessionVariables();
          
                      }, function errorCallback(response) {
                          console.log("Error will retriving data");
                      });
                  }
        
        $scope.getAllServices  = function(){

          ServiceStoresService.GetAll(function(resp){
            vm.allServiceStores = resp.data;
          });
        }
        $scope.checkServicePresent = function(id){
            if(!$scope.selectedTeam.serviceStores)
              return false;
            var filtered=$filter('filter')( $scope.selectedTeam.serviceStores, function(d){ return d.id===id})
            if(filtered.length==0)
              return false;

            return true ;
        }
        $scope.clearServiceStore = function(serviceStore){
          serviceStore.warning = false;
          serviceStore.error = false;
          serviceStore.termsAndConditions = false;
        }

        $scope.setSelectedTeam = function(url){
          localStorage.selectedTeamId = $scope.selectedTeam.id;
          window.location.href=url;
        }
        $scope.addService = function(serviceStore,modalName){
          console.log("In Add");
          console.log(serviceStore);
          console.log(modalName)
          if(!serviceStore.termsAndConditions){
            serviceStore.warning = true;
            return;
          }
          ServiceStoresService.Create($scope.selectedTeam.id,serviceStore.id,function(resp){
            $scope.selectedTeam.serviceStores=$scope.selectedTeam.serviceStores?$scope.selectedTeam.serviceStores:{}
            $scope.selectedTeam.serviceStores.push(resp.data)
              serviceStore.warning = false;
              $('#modal-confirm-service-'+serviceStore.id).modal('hide');
              $('#modal-success-team-service-add').modal('show');
            }, function(resp){
              serviceStore.error = true;
            }

          )
        }
        $scope.signout = function(){
          $window.localStorage.clear();
           $rootScope.globals = {};
        };
    }

})();
