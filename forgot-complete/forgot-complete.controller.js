(function () {
    'use strict';

    angular.module('app')
        .controller('ForgotCompleteController', ForgotCompleteController);

    ForgotCompleteController.$inject = ['$location','$routeParams', 'PasswordService', 'FlashService'];
    function ForgotCompleteController($location,$routeParams, PasswordService, FlashService) {
        var vm = this;

        var password="";
        vm.password=password;

        vm.key=$routeParams.key;

        var completeForgotPassword = function(){
          console.log("here")
          var isValidForm = $('#forgot-init-form').data('formValidation').isValid()
          if (isValidForm){
            console.log(this.username);
            PasswordService.completeForgotPassword(this.key,this.password).then(
              function(response){
                $(".alert-danger").fadeOut()
                $(".alert-success").fadeIn();

                window.setTimeout(function () {
                  location.href = "/#!login";
                }, 3000);
              },
              function(response){
                $(".alert-danger").fadeOut()
                $(".alert-success").fadeIn();
              }
            )

          }
        }

        vm.completeForgotPassword=completeForgotPassword

    }

})();
