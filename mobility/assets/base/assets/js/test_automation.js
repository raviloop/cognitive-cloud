$('.btnShowList').on('click', function (e) {
    $(this).closest(".memberList").find("")
    $('.memberListing').toggleClass('slide');
});


$('.btnAddMember').on('click', function (e) {
    var arr = [];
    var eleParent = $(this).closest(".memberListing");
    
    eleParent.find('input:checked').each(function () {
        var labeltxt = $(this).siblings('label').text();
        var parent = $(this).parents('.list-group-item');
        parent.remove();
        arr.push(labeltxt);
    });

    var blockDiv = $(this).closest(".addUserBlock");
    $.grep(arr, function (k, v) {
        var clone = blockDiv.find('.memberListGroup li:last-child').clone();
        clone.find('.personName').text(k);
        clone.appendTo(blockDiv.find('.memberListGroup'));
    });
    eleParent.toggleClass('slide');
});


//
$('.memberListGroup').on('click', '.btn-del', function (e) {
    e.preventDefault();
    var parentLi = $(this).closest('li');
    var mem = parentLi.find('.personName').text();

    var blockDiv = $(this).closest(".addUserBlock");
    var memberLength = blockDiv.find('.memberListing li').length;



    var clone = blockDiv.find('.memberListing li:last-child').clone();
    clone.find('label').text(mem);
    clone.find('label').attr("for","inputChecked"+(memberLength+10));
    clone.find('input[type="checkbox"]').attr("id","inputChecked"+(memberLength+10));
    clone.appendTo(blockDiv.find('.memberListing ul'));
    parentLi.remove();
    console.log(mem);
});

var invokerForSteps;
$('#modalDeleteSteps').on('show.bs.modal', function (e) {
  invokerForSteps = $(e.relatedTarget); 
});
$('#btndeleteStepConfirm').on("click", function () {
    invokerForSteps.closest(".panel").remove();
    swal({
        title: "Confirmed",
        text: "Step deleted successfully",
        type: "success",
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Done',
        timer: 3000
    });
});

var invokerForTestScript;
$('#modalDeleteTestScript').on('show.bs.modal', function (e) {
  invokerForTestScript = $(e.relatedTarget); 
});
$('#btndeleteTestScriptConfirm').on("click", function () {
    invokerForTestScript.closest(".panel").remove();
    swal({
        title: "Confirmed",
        text: "Test Script deleted successfully",
        type: "success",
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Done',
        timer: 3000
    });
});

var invokerForTestSuite;
$('#modalDeleteTestSuite').on('show.bs.modal', function (e) {
  invokerForTestSuite = $(e.relatedTarget); 
});
$('#btndeleteTestSuiteConfirm').on("click", function () {
    invokerForTestSuite.closest(".panel").remove();
    swal({
        title: "Confirmed",
        text: "Test Suite deleted successfully",
        type: "success",
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Done',
        timer: 3000
    });
});





/*****20170904 */
$(document).on("click",".btnRoleAction",function() {
    var eleParent = $(this).closest("tr");
    eleParent.find("input").attr("disabled",false);
});

$(document).on("change", ".roleCheckBox input", function () {
    if ($(this).prop("checked") == true) {
        $(this).parent().find("i").addClass("rActive");
        $(this).parent().find("i").css("color","rgb(34, 182, 110)");
    }
    else {
        $(this).parent().find("i").removeClass("rActive");
        $(this).parent().find("i").css("color","rgb(239, 25, 60)");
    }
});




$("#startNewProject").click(function() {
	$(".secSelectProject").hide(function(){
		$(".newProject").fadeIn();
	});
});

$(document).on("change","#testAutomationProject",function(){
	$(".wizard-buttons > a[data-wizard='next']").click();

    /***show predefined ts */
    $("#tsstep2 .existingTestSuite").show();
    $("#tsstep2 .newTestSuite").hide();
});

$(document).on("click","#btnCreateNewTestSuite",function() {
    $(".newTestSuite").slideDown();
});


/**add steps */

$(document).on("click","#btnAddSteps",function() {
	
	var cloneparent = $("#pg_Steps");
	var eleToClone = cloneparent.find(".panel:first-child");
	var clonedElem = eleToClone.clone();

	var newCloneNum = cloneparent.find(".panel").length + 1;
	clonedElem.find("#stepNo").html(newCloneNum);
	clonedElem.find(".btnAction").attr("data-target","#actStep"+newCloneNum).attr("aria-controls","#actStep"+newCloneNum);
	clonedElem.find(".panelActions").attr("id","actStep"+newCloneNum);

    // clonedElem.find(".actionType").select2();
    // clonedElem.find(".actionType").select2("destroy");
    // clonedElem.find(".actionType").select2();


	cloneparent.append(clonedElem);

});






/***steps dropdown logic */

$(document).on("change","#actionType",function() {
	var actionVal = $(this).val();

	var actParent = $(this).parents(".actionRow");
	$(".hideEle").hide();
	switch(actionVal) {
	 case "Go to URL":
	  actParent.find(".eleURLName").show();
	  break;
	 case "Add Assertion":
	  actParent.find(".elelocatorType").show();
	  break;
	 case "API":
	  actParent.find(".elelocatorType").show();
	  break;
	  case "Check":
	  actParent.find(".elelocatorType").show();
	  break;
	  case "Wait":
	  actParent.find(".elewaitDuration").show();
	  break;
	 default:
	  $(".hideEle").hide();
	}
});

$(document).on("change","#locatorType",function() {
	var locatorVal = $(this).val();

	var actParent = $(this).parents(".actionRow");
	$(".hideEle").hide();
	$(this).closest(".form-group").show();
	switch(locatorVal) {
	 case "Id":
	  actParent.find(".elelocatorNameVal").show();
	  break;
	 case "Name":
	  actParent.find(".elelocatorIdVal").show();
	  break;
	 default:
	  $(".hideEle").hide();
	  $(this).closest(".form-group").show();
		
	}
});


/**select2 scroll problem */

$('select').on('select2:open', function (evt) {
    $("html,body").css("overflow-y","hidden");
});
$('select').on('select2:close', function (evt) {
    $("html,body").css("overflow-y","auto");
});







