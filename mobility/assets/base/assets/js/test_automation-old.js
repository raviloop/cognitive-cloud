

var tagLabels = "";
/***show app div on select mobile***/
$(".selectAppType").change(function() {
	var selectAppTypeVal = $(this).val();
	if(selectAppTypeVal === "Mobile") {
		$(".selectOsDiv").fadeIn();
	}
	else
	{
		$(".selectOsDiv").fadeOut();
	}
});

/*****tags generator****/
//var previousaddtagsInputVal = "";
//$(".addtagsInput").click(function() {
//	var addtagsInputVal = $(this).val();
//	if(addtagsInputVal.trim() != "") {
//		if(previousaddtagsInputVal != addtagsInputVal) {
//			var tagElement = '<label class="tagLabel">'+addtagsInputVal+'<span class="pull-right btnRemovetags">x</span></label>';
//			$(".generatedTags").append(tagElement);

//		}
//	}
//	previousaddtagsInputVal = addtagsInputVal;
//});

//$(".addtagsInput").keypress(function(e) {
//    if(e.which == 13) {
//        $(this).trigger("click");
//    }
//});

// Pravin - 7-June-17 ------ tags generator
var previousaddtagsInputVal = "";
$('#btnAddTag').on('click', function (e) {
    //e.preventDefault();
    var txtTag = $("#txtTagInput");
    var addtagsInputVal = txtTag.val();
    if (addtagsInputVal.trim() != "") {
        if (previousaddtagsInputVal != addtagsInputVal) {
            var tagElement = '<label class="tagLabel" data-tag="'+ addtagsInputVal +  '">' + addtagsInputVal + '<span class="pull-right btnRemovetags">x</span></label>';
            $(".generatedTags").append(tagElement);
            txtTag.val('');
        }
    }
    previousaddtagsInputVal = addtagsInputVal;
});

$("#txtTagInput").keypress(function (e) {
    if (e.which == 13) {
        e.preventDefault();
        $('#btnAddTag').trigger("click");
    }
});


$(document).on("click",".btnRemovetags",function() {
	$(this).parent().remove();
});

$(document).on("click","#generatedTags2 .tagLabel",function() {
	$(this).toggleClass("tagLabelActive");
})







/***standard roles****/
//screen2
$(".createrolewrap .md-input").keyup(function() {
  var roleName = $(".roleName").val();
  var roleAliasFieldVal = $(".roleAliasField").val();

  if((roleName.length > 0) && (roleAliasFieldVal.length > 0 )) {
    $(".roleAssignmentDiv").slideDown();
  }
});

//checkbox

$(document).on("change", ".rolePowerCheckBox input", function () {
    if (!$(this).is(':checked')) {
        //false
        $(this).parent().find("i").removeClass("ion-checkmark");
        $(this).parent().find("i").addClass("ion-close");
        $(this).parent().find("i").css("color", "#ef193c");
    }
    else {
        $(this).parent().find("i").removeClass("ion-close");
        $(this).parent().find("i").addClass("ion-checkmark");
        $(this).parent().find("i").css("color", "#22b66e");
    }
});

$(document).on("click",".btnRoleAction",function(){
  $(this).closest("tr").find(".rolePowerCheckBox input").attr("disabled",false);
  $(this).closest("tr").find(".rolePowerCheckBox i").css("color", "#ef193c");
})

$(document).on("ready",function(){
  $(".rolePowerCheckBox input").attr("disabled","true");

  setTags();
})


/*add new role btn***/

$(".addNewRoleBtn > a").click(function() {
  $(".liAddRole").slideDown();
})
$(".saveNewRoleBtn").click(function() {
  var roleNewName = $(this).parent().find(".roleName").val();
  if(roleNewName != "") {
  $(".liAddRole").hide();
  var roleCount = $(".ulRoleNames ul.nav").children().length -1;
  var nextNumber = roleCount + 1;

  var roleLeftMenu = '<li class="nav-item"><a href="#left-tab' + nextNumber + '" data-role-name="' + roleNewName + '" class="nav-link" data-toggle="tab"  role="tab"><span>' + roleNewName + '</span><i class="ion-ios-trash-outline pull-right delNewUser"></i></a></li>';
  $(".liAddRole").before(roleLeftMenu);
  $(this).parent().find(".roleName").val("");

  var roleTabContent = $(".role_tab_pane_dynamic").clone();
  roleTabContent.removeClass("role_tab_pane_dynamic");
  roleTabContent.prop('id', 'left-tab'+nextNumber);
  roleTabContent.find(".roleAssignmentDiv p > strong").html(roleNewName);

  $(".divRoleDesc").append(roleTabContent);

  }
});


$(document).on("click",".delNewUser", function(e) {
  e.stopPropagation();
  $(this).closest("li").slideUp();
})








$(document).on({
    mouseenter: function () {
        $(this).find(".card-block p").stop().slideDown(300);
    },
    mouseleave: function () {
        $(this).find(".card-block p").stop().slideUp(300);
    }
}, ".memberAddedList > ul > li .card");



/***template-form*****/
var fieldStatus = 0;
$(".fieldIcon").click(function() {
  $(this).find("i").toggleClass("ion-eye-disabled");
  if(fieldStatus === 0) {
    $(this).parent().find(".form-control").attr("disabled",true);
    fieldStatus = 1;
  }
  else
  {
    $(this).parent().find(".form-control").attr("disabled",false);
    fieldStatus = 0;
  }

});

var dynamicFieldStatus = 0
$(document).on("click",".btnFieldStatus",function() {
  $(this).toggleClass("ion-eye-disabled");

  if(dynamicFieldStatus === 0) {
    $(this).closest(".form-field").find(".form-control").attr("disabled",true);
    dynamicFieldStatus = 1;
  }
  else
  {
    $(this).closest(".form-field").find(".form-control").attr("disabled",false);
    dynamicFieldStatus = 0;
  }
});

/***add member******/
$(document).on("click",".userSelectBtn",function() {
  $(".card").css("z-index",21);
  $(this).closest(".card").css("z-index",25);
  $(this).closest(".addMemberGrp").toggleClass("active");
  $(this).parent(".addMemberGrp").find("input[type=checkbox]").attr('checked', false).removeClass("memActive");
});



///// commented by Pravin - 08-06-17 because data is generated dynamically - see below for new updated function
//$(document).on("click",".addMember",function() {
//  $(this).closest(".addMemberGrp").toggleClass("active");

//  var memLength = $(this).closest(".addUserContent").find(".memberAddedList > ul > li").length;
//  $(this).closest(".addMemberGrp").find('input[type="checkbox"]').each(function () {

//      if ($(this).hasClass('memActive')) {
//          var activeMemName = $(this).parent().find("span").html();
//          var memberLi = '<li class="col-lg-2 col-sm-4 col-xs-6"><div class="card"><span><i class="ion-ios-person"></i></span><div class="card-block"><h6 class="card-title">' + activeMemName + '</h6><p style="display: block; height: 37px; padding-top: 6.4px; margin-top: 0px; padding-bottom: 3.2px; margin-bottom: 0px; overflow: hidden;"><a href="javascript:;" class="btn-del pull-left"><i class="ion-edit"></i></a><a href="javascript:;" class="btn-del pull-right" data-toggle="modal" data-target="#modal-deletemember"><i class="ion-ios-trash-outline"></i></a></p></div></div></li>';
//          $(".addMember").closest(".addUserContent").find(".memberAddedList > ul").append(memberLi);
//          memLength = memLength + 1;
//          $(".addMember").closest(".addUserHeader").find("p span").html(memLength);
//      }
//  });
//});

//$('.addMemberGrp input[type="checkbox"]').change(function () {
//    if ($(this).is(':checked')) {
//        $(this).addClass("memActive");
//    }
//});

// Pravin - 08-06-17 - event handler
$(document).on('click', '.addMember', function (e) {
    var tabPane = $(this).parents('.tab-pane');
    var addMemberGrp = $(".addMemberGrp", tabPane);
    var addUserContent = $(".addUserContent", tabPane);

    addMemberGrp.toggleClass("active");

    var memLength = addUserContent.find(".memberAddedList > ul > li").length;
    addMemberGrp.find('input[type="checkbox"]').each(function () {
        if ($(this).is(':checked')) {
            var activeMemName = $(this).val();
            var userid = $(this).data('userid');
            var memberLi = '<li class="col-lg-2 col-sm-4 col-xs-6"><div class="card"><span><i class="ion-ios-person"></i></span><div class="card-block"><h6 class="card-title capitalize " data-userid="' + userid +'" >' + activeMemName + '</h6><p style="display: block; height: 37px; padding-top: 6.4px; margin-top: 0px; padding-bottom: 3.2px; margin-bottom: 0px; overflow: hidden;"><a href="javascript:;" class="btn-del pull-left"><i class="ion-edit"></i></a><a href="javascript:;" class="btn-del pull-right" data-toggle="modal" data-target="#modal-deletemember"><i class="ion-ios-trash-outline"></i></a></p></div></div></li>';
            addUserContent.find(".memberAddedList > ul").append(memberLi);
            memLength = memLength + 1;
            $(".addUserHeader", tabPane).find("p span").html(memLength);
        }
    });
});



$(".date .form-control").click(function() {
  $(this).parent().find(".input-group-addon").click();
})




/***show selected action div in screen3 steps list***/
$(document).on("change",".selectActionType",function() {
	var selectActionTypeVal = $(this).val();
	$(this).closest("li").find(".selectLocatorType").val("");
	$(this).closest("li").find(".actionSelectedType").hide();
    
    if(selectActionTypeVal == "launch"){
        $(this).closest("li").find(".action_Url").css("display","flex");
    } else if(selectActionTypeVal != "launch"){
         $(this).closest("li").find(".action_locator").css("display","flex");
    } else{
        $(this).closest("li").find(".actionSelectedType").fadeOut();
    }
    $(this).closest("li").find(".locatorInputVal").fadeOut();
    
/*	switch(selectActionTypeVal) {
	 case "Launch":
	  
	  break;
	 case "attach":
	  $(this).closest("li").find(".action_locator").css("display","flex");
	  break;
    case "attach":
	  $(this).closest("li").find(".action_locator").css("display","flex");
	  break;
    case "cancel":
	  $(this).closest("li").find(".action_locator").css("display","flex");
	  break;

	 case "click":
	  $(this).closest("li").find(".action_locator").css("display","flex");
	  break;
    case "optionalClick":
	  $(this).closest("li").find(".action_locator").css("display","flex");
	  break;
    case "clickCheckBox":
	  $(this).closest("li").find(".action_locator").css("display","flex");
	  break;
            
	 default:
	  $(this).closest("li").find(".actionSelectedType").fadeOut();
	}*/
	

});


/**locator select*****/
$(document).on("change",".selectLocatorType",function() {
	var selectLocatorTypeVal = $(this).val();
    
    if(selectLocatorTypeVal.length > 0){
       $(this).closest(".action_locator").find(".locatorInputVal").css("display","block");
	   $(this).closest(".action_locatorData").find(".locatorInputVal").css("display","block");
    } else {
         $(this).closest(".action_locator").find(".locatorInputVal").fadeOut();
    }
    
	/*switch(selectLocatorTypeVal) {
	 case "Id":
	  $(this).closest(".action_locator").find(".locatorInputVal").css("display","block");
	  $(this).closest(".action_locatorData").find(".locatorInputVal").css("display","block");
	  break;
	 case "Name":
	  $(this).closest(".action_locator").find(".locatorInputVal").css("display","block");
	  $(this).closest(".action_locatorData").find(".locatorInputVal").css("display","block");
	  break;
	 default:
	  $(this).closest(".action_locator").find(".locatorInputVal").fadeOut();
	}*/
});

/**addnewStep**/

$(".btnAddNewStep").click(function() {
	var stepCount = $(".addSteps > ul > li").length;
	var newStepContent = $(".addSteps > ul > li:last-child").clone();
	newStepContent.find(".stepNo > span").html(stepCount+1);
	newStepContent.find(".form-control").val("");
	newStepContent.find(".action_locator").hide();
	newStepContent.find(".action_locatorData").hide();
	$(".addSteps > ul").append(newStepContent);
});

/***delete steps**/

/*$(".stepBtnDel").click(function() {

});*/

/**addnewStep**/

$(".btnAddTestScript").click(function() {
	var tsCount = $(".addTestScripts .card").length;
	var nexttsCount = tsCount+1;
	var newTsContent = $(".addTestScripts .card:last-child").clone();
	newTsContent.find(".card-header h6 > a").attr("href" , "#testScript"+nexttsCount);
	newTsContent.find(".card-header h6 > a").html("Demo script "+nexttsCount);
	newTsContent.find(".collapse").attr("id" , "testScript"+nexttsCount);
	$(".addTestScripts > .contentTestScripts").append(newTsContent);
});

function showTsAlert(num) {

	if(num === 1) {
		var projectName = $("#projectname").val();
		if(projectName === "") {
			$("#fLabelPname").addClass("fLabel");
			$("#fLabelPname").parent().find("input").focus();
			return false;
		}
		else
		{
			$("#fLabelPname").removeClass("fLabel");
		}
	}
	else if(num === 2) {
		var tsName = $("#tsName").val();
		if(tsName === "") {
			$("#fLabeltsName").addClass("fLabel");
			$("#fLabeltsName").parent().find("input").focus();
			return false;
		}
		else
		{
			$("#fLabeltsName").removeClass("fLabel");
		}
		var moduleName = $("#moduleName").val();
		if(moduleName === "") {
			$("#fLabelmoduleName").addClass("fLabel");
			$("#fLabelmoduleName").parent().find("input").focus();
			return false;
		}
		else
		{
			$("#fLabelmoduleName").removeClass("fLabel");
		}
	}




	var alertDiv = document.createElement('div');
	var alertDivContent = "";
	var pglocation = "";
	$(alertDiv).addClass("alert").addClass("alert-success");
	$(alertDiv).attr("role", "alert");

	switch(num) {
	 case 1:
	  alertDivContent = "Project created successfully";
	  pglocation = "test_automation2.html";
	  break;
	 case 2:
	  alertDivContent = "Test Suite created successfully";
	  pglocation = "test_automation3.html";
	  break;
	 case 3:
	  alertDivContent = "Test Scripts created successfully";
	  pglocation = "test_automation4.html";
	  break;
	  case 4:
	  alertDivContent = "New Test suit created successfully";
	  pglocation = "test_automation4.html";
	  break;
	 default:
	  $(alertDiv).hide();
	}

	$(alertDiv).append("<p>"+alertDivContent+"</p>");

	$(".contentTa").prepend($(alertDiv));
	$(alertDiv).fadeIn();

	if (typeof(Storage) !== "undefined") {
		generatedTagsContent = $(".generatedTags").html();
		sessionStorage.setItem("tagLabels", generatedTagsContent);
	}


	window.setTimeout(function () {
		$(alertDiv).fadeOut().remove();
		location.href = pglocation;
	}, 6000);
}



function setTags() {
	var createdTags = sessionStorage.getItem("tagLabels");

	var tagsDivid = $(".generatedTags").attr("id");

	if(tagsDivid === "generatedTags1") {
		$(".generatedTags").html();
	}
	else if(tagsDivid === "generatedTags2") {
		$(".generatedTags").html(createdTags);
		if($(".generatedTags .tagLabel").length > 0) {
			$(".labelDiv").show();
		}
		else
		{
			$(".labelDiv").hide();
		}
	}
	else if(tagsDivid === "generatedTags3") {
		$(".generatedTags").html(createdTags);
		if($(".generatedTags .tagLabel").length > 0) {
			$(".labelDiv").show();
			$(".labelDiv > label").html("Selected Tags");
		}
		else
		{
			$(".labelDiv").hide();
		}
		$(".tagLabel").each(function() {
			if($(this).hasClass("tagLabelActive")) {
				$(this).show();
				$(this).find(".btnRemovetags").remove();
			}
			else
			{
				$(this).hide();
			}
		})
	}
	else if(tagsDivid === "generatedTags4") {
		$(".generatedTags").html(createdTags);
		if($(".generatedTags .tagLabel").length > 0) {
			$(".labelDiv").show();
			$(".labelDiv > label").html("Selected Tags");
		}
		else
		{
			$(".labelDiv").hide();
		}
		$(".tagLabel").each(function() {
			if($(this).hasClass("tagLabelActive")) {
				$(this).show();
				$(this).find(".btnRemovetags").remove();
			}
			else
			{
				$(this).hide();
				$(".labelDiv").hide();
			}
		})
	}


}


$(".btnRefreshNewTs").click(function(){

	var alertDiv = document.createElement('div');
	var alertDivContent = "";
	var pglocation = "";
	$(alertDiv).addClass("alert").addClass("alert-success");
	$(alertDiv).attr("role", "alert");
	$(alertDiv).append("<p>Test Script created successfully</p>");
	$(".contentTa").prepend($(alertDiv));
	$(alertDiv).fadeIn();

	window.setTimeout(function () {
		$(alertDiv).fadeOut().remove();


		var stepCount = $(".addSteps > ul > li").length;
		var newStepContent = $(".addSteps > ul > li:last-child").clone();
		newStepContent.find(".stepNo > span").html("1");
		newStepContent.find(".form-control").val("");
		$(".addSteps > ul").html("");
		$(".addSteps > ul").append(newStepContent);


	}, 6000);
});


$(".btnTsCopy").click(function() {
	var copiedTab = $(this).closest(".contentTestScripts").clone();
	copiedTab.find(".card-header ")
	copiedTab.insertAfter($(this).closest(".contentTestScripts"));

});

function SaveDataToLocalStorage(data)
{

    var a = [];
    // Parse the serialized data back into an aray of objects
    a = JSON.parse(localStorage.getItem('session'));
    // Push the new data (whether it be an object or anything else) onto the array
    a.push(data);
    // Re-serialize the array back into a string and store it in localStorage
    localStorage.setItem('session', JSON.stringify(a));
}
/***move to pages***/
function showPage(num) {

	window.location.href="test_automation"+num+".html";
}

$(document).on("ready",function() {
	var pgArray =  JSON.parse(localStorage.getItem('session'));
	for(var i = 1; i<= pgArray.length; i++)
	{
		$("#a_tab"+i).attr("href","test_automation"+i+".html");
		$("#a_tab"+i).attr('onClick','showPage('+i+')');
		$("#a_tab"+i).closest("li").addClass("completedStep");


		
	}
})
