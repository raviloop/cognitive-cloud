﻿(function () {
    'use strict';

    angular
        .module('app', ['ngRoute', 'ngCookies'])
        .config(config)
        .run(run);

    config.$inject = ['$routeProvider', '$locationProvider'];
    function config($routeProvider, $locationProvider) {

        $routeProvider
            .when('/', {
                controller: 'LandingController',
                templateUrl: 'landing/landing.view.html',
                controllerAs: 'vm'
            })

			       .when('', {
                controller: 'LandingController',
                templateUrl: 'landing/landing.view.html',
                controllerAs: 'vm'
            })

            .when('/login', {
                controller: 'LoginController',
                templateUrl: 'login/login.view.html',
                controllerAs: 'vm'
            })

            .when('/forgot-init', {
                controller: 'ForgotInitController',
                templateUrl: 'forgot-init/forgot-init.view.html',
                controllerAs: 'vm'
            })

            .when('/forgot-complete/:key', {
                controller: 'ForgotCompleteController',
                templateUrl: 'forgot-complete/forgot-complete.view.html',
                controllerAs: 'vm'
            })

            .when('/register', {
                controller: 'RegisterController',
                templateUrl: 'register/register.view.html',
                controllerAs: 'vm'
            })

			.when('/home', {
                controller: 'HomeController',
                templateUrl: 'home/home.view.html',
                controllerAs: 'vm'
            })
            .when('/teamselector', {
                      controller: 'TeamSelectorController',
                      templateUrl: 'teamselector/teamselector.view.html',
                      controllerAs: 'vm'
                  })
            .when('/control-center', {
                      controller: 'ControlCenterController',
                      templateUrl: 'control-center/control-center.view.html',
                      controllerAs: 'vm'
                  })
            .when('/plans', {
                controller: 'PlansController',
                templateUrl: 'plans/plans.view.html',
                controllerAs: 'vm'
            })

			.when('/userdashboard', {
                controller: 'UserDashboardController',
                templateUrl: 'user_dashboard/user_dashboard.html',
                controllerAs: 'vm'
            })


            .otherwise({ redirectTo: '/' });

    }

    run.$inject = ['$rootScope', '$location', '$cookies', '$http'];
    function run($rootScope, $location, $cookies, $http) {
        // keep user logged in after page refresh
        $rootScope.globals = $cookies.getObject('globals') || {};
        $rootScope.sessionGlobals = $cookies.getObject('sessionGlobals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata;
        }

        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            // redirect to login page if not logged in and trying to access a restricted page
            var restrictedPage=true;
            restrictedPage = $.inArray($location.path(), ['/login', '/register','/','','/plans','/forgot-init']) === -1;
            restrictedPage =restrictedPage && (( $location.path().indexOf("/forgot-complete"))===-1)
            //restrictedPage=false;
            var loggedIn = $rootScope.globals.currentUser;
            if (restrictedPage && !loggedIn) {
                $location.path('/');
            }
        });
    }

})();
