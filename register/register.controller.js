(function () {
    'use strict';

    angular.module('app')
        .controller('RegisterController', RegisterController);

    RegisterController.$inject = ['UserService', '$location', '$rootScope', 'FlashService'];
    function RegisterController(UserService, $location, $rootScope, FlashService) {
        var vm = this;

        vm.register = register;
        vm.user = {};
        vm.user.headerBgColor = '#000000';
        vm.user.navigationBgColor = '#000000';

        function register() {
                var isValidForm = $('#register').data('formValidation').isValid();
                if (isValidForm) {
                        console.log("vinayakhere")
                        UserService.Create(vm.user)
                            .then(function (response) {
                                //if (response.success) {
                                console.log(response);
                                if (response.data.status == "success") {
                                    //FlashService.Success('Registration successful', true);
                                     $(".alert-danger").fadeOut();
                                    $(".alert-success").fadeIn();
                                    window.setTimeout(function () {
                                      location.href = "/#!landing";
                                    }, 3000);
                                    console.log("vinayakhere2")
                                    //$location.path('/#login');
                                } else {
                                    console.log("vinayakhere Else" + response.data.message);
                                    vm.errorMsg=response.data.message;
                                    $(".alert-danger").fadeIn();
                                  //  FlashService.Error(response.data.message);
                                }
                            },
                          function(response){
                            console.log(response);
                            vm.errorMsg=response.data.message;
                            $(".alert-danger").fadeIn();
                            console.log("Failed Vinayak Else" + response.data.message);
                          //  FlashService.Error(response.data.message,true);
                          //  $location.path('/#register');
                          });
                    }
                    else
                    {
                        console.log("not valid");
                    }
                }
    }
})();
