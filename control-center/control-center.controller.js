(function() {
    'use strict';

    angular.module('app')
        .controller('ControlCenterController', ControlCenterController);

    ControlCenterController.$inject = ['TeamService','ServiceStoresService', 'UserService', '$rootScope', '$scope', '$window'];


    function ControlCenterController(TeamService,ServiceStoresService, UserService, $rootScope, $scope, $window) {
        var vm = this;
        vm.user = null;
        vm.allUsers = [];

        initController();

        function initController() {
            loadCurrentUser();
        }


        //Create Team, Edit Team, Delete Team

        $scope.team = [];
        $scope.myNumber = 5;

        $scope.getNumber = function(num) {
            //console.log("getNumber Was Called "+num);
            return new Array(num);
        }

        $scope.getAllTeam = function() {

            console.log("get team data was called");
            $scope.allTeamData = [];

            TeamService.GetAllWithRole().then(function successCallback(response) {
                console.log("Success");
                console.log(response);
                $scope.allTeamData = response.data;
                $scope.allTeamDataBkp = angular.copy($scope.allTeamData);
                //console.log(angular.toJson($scope.allTeamData));
                //console.log(angular.toJson($scope.allTeamData));
            }, function errorCallback(response) {
                console.log("Error will retriving data");
            });
        }

        $scope.resetTeamsEdit = function() {
            $scope.allTeamData = angular.copy($scope.allTeamDataBkp);
        }
        $scope.accordiontoggle = function (index){
            
            $("#accordion-control-"+index).toggle("collapse");
        }
        $scope.makeTeamEditable = function(data, index) {
          $("#accordion-control-"+index).collapse();
          data.isTeamEnabled = !data.isTeamEnabled;
        }

        $scope.editTeam = function(index, teamId) {
            console.log($scope.allTeamData);

            var team = $scope.allTeamData[index];
            TeamService.Update(team, function(response){
              $window.location.reload();
            });
        }

        $scope.addTeamUser = function (index){
                    $scope.allTeamData[index].teamUsers.push({});
        }

        $scope.removeTeamUser = function (index, teamuser){
                    $scope.allTeamData[index].teamUsers.splice($scope.allTeamData[index].teamUsers.indexOf(teamuser),1);
        }

        $scope.deleteTeam = function(index) {
            event.preventDefault();
            TeamService.Delete(index).then(function successCallback(response) {
                console.log("Success");
                console.log(response);
                $window.location.reload();
            }, function errorCallback(response) {
                console.log("Error will retriving data");
            });
        }

        function loadCurrentUser() {
            UserService.GetByUsername($rootScope.globals.currentUser.username)
                .then(function (user) {
                    vm.user = user.data;
                    if(vm.user.authorities[0]==="ROLE_ADMIN"){
                      vm.isAdmin = true;
                      vm.isUser=false;
                    }
                    else{
                      {
                        vm.isAdmin = false;
                        vm.isUser=true;
                      }
                    }
                    $scope.getAllServices();
                });

        };

        $scope.getAllServices  = function(){

          ServiceStoresService.GetAll(function(resp){
            vm.allServiceStores = resp.data;
          });
        }
        $scope.checkServicePresent = function(id){
          return false;
        }
        $scope.clearServiceStore = function(serviceStore){
          serviceStore.warning = false;
          serviceStore.error = false;
          serviceStore.termsAndConditions = false;
        }

        $scope.addService = function(serviceStore,modalName){
          console.log("In Add");
          console.log(serviceStore);
          console.log(modalName)
          if(!serviceStore.termsAndConditions){
            serviceStore.warning = true;
            return;
          }
          ServiceStoresService.Create(null,serviceStore.id,function(resp){

              serviceStore.warning = false;
              $('#modal-confirm-service-'+serviceStore.id).modal('hide');
              $('#modal-success-team-service-add').modal('show');
            }, function(resp){
              serviceStore.error = true;
            }

          )
        }

        $scope.signout = function() {
            $window.localStorage.clear();
            $rootScope.globals = {};
        };
    }

})();
