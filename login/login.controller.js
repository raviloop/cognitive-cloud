(function () {
    'use strict';

    angular.module('app')
        .controller('LoginController', LoginController);

    LoginController.$inject = ['$location', 'AuthenticationService', 'FlashService','UserService'];
    function LoginController($location, AuthenticationService, FlashService,UserService) {
        var vm = this;

        vm.login = login;

        (function initController() {
            // reset login status
            AuthenticationService.ClearCredentials();
        })();

        function login() {
          var isValidForm = $('#loginForm').data('formValidation').isValid();
          if (isValidForm) {
            vm.dataLoading = true;
            AuthenticationService.Login(vm.username, vm.password, function (response) {

                if (response!=null) {
                    AuthenticationService.SetCredentials(vm.username, vm.password,response.data.access_token);
                        UserService.GetByUsername(vm.username)
                        .then(function (response) {
                            vm.userRole=response.data.authorities[0];
                            console.log("ROLE:--"+vm.userRole);
                            if(vm.userRole==='ROLE_ADMIN')
                             $location.path('/control-center');
                         else
                            $location.path('/userdashboard');
                        });


                } else {
                    FlashService.Error(response.message);
                    vm.dataLoading = false;
                }
            },
          function(response){
            console.log("Invalid");
            $(".alert-danger").fadeIn();
            window.setTimeout(function () {
              $(".alert-danger").fadeOut();
            }, 3000);
          });

          }

        };
    }

})();
